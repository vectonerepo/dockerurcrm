﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdatecompanydeviceorderreturnModel
    {
        public class UrcrmportalupdatecompanydeviceorderreturnInput
        {

            public int company_id { get; set; }
            public int order_id { get; set; }
            public int device_approval { get; set; }
            public string comments { get; set; }
            public string tracking_no { get; set; }
        }
        public class UrcrmportalupdatecompanydeviceorderreturnOutput:ErrorMessageModel
        {
            
        }
    }
}
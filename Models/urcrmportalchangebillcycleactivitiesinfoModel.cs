﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalchangebillcycleactivitiesinfoModel
    {
        public class urcrmportalchangebillcycleactivitiesinfoInput
        {
            public int company_id { get; set; }
            public int bill_cycle { get; set; }
            public int id { get; set; }
            public int processtype { get; set; }
            public string plan_subscribe { get; set; }
            public string reference_no { get; set; }
            public string bundle_cycle_description { get; set; }
            public double? bundle_cycle_charge { get; set; }
            public double? adjustment_charge { get; set; }
            public double? tax_fee { get; set; }
            public int tax_per { get; set; }
            public double? total_bundle_cycle_charge { get; set; }
            public int? userid { get; set; }
        }
        public class urcrmportalchangebillcycleactivitiesinfoOutput: ErrorMessageModel
        {
            //public DateTime? next_bill_date { get; set; }
            public int? bill_cycle { get; set; }
            public int? cp_id { get; set; }
            public string next_bill_date { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcompanydeviceorderreturngetModel
    {

        public class UrcrmportalcompanydeviceorderreturngetInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmportalcompanydeviceorderreturngetOutput:ErrorMessageModel
        {
            public int id { get; set; }
            public int order_id { get; set; }
            public string prod_code { get; set; }
            public string prod_name { get; set; }
            public string prod_model { get; set; }
            public DateTime rtn_date { get; set; }
            public int rtn_quantity { get; set; }
            public double rtn_charge { get; set; }
            public string rtn_reason { get; set; }
            public int rtn_status { get; set; }
            public string tracking_no { get; set; }
            public string comments { get; set; }
            public int company_id { get; set; }
            public string company_name { get; set; }
            public string status_desc { get; set; }
        }
    }
}
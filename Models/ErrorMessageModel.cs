﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class ErrorMessageModel
    {
        public string Message { get; set; }
        public int? Code { get; set; }
    }
}
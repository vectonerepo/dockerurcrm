﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalsmesubscribebundleespModel
    {

        public class UrcrmportalsmesubscribebundleespInput
        {
            public int? company_id { get; set; }
            public string sitecode { get; set; }
            public int? enterpriseid { get; set; }
            public int? bundleid { get; set; }
            public int? paymode { get; set; }
            public string processby { get; set; }
            public string bundle_description { get; set; }
            public double? bundle_charge { get; set; }
            public int? userid { get; set; }
            public double? adjustment_charge { get; set; }
            public double? tax_fee { get; set; }
            public int tax_per { get; set; }
            public double? total_bundle_charge { get; set; }
            public string reference_no { get; set; }
        }
        public class UrcrmportalsmesubscribebundleespOutput : ErrorMessageModel
        {

        }
    }
}
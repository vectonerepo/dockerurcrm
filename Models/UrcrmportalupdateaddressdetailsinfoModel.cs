﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdateaddressdetailsinfoModel
    {
        public class UrcrmportalupdateaddressdetailsinfoInput
        {
            public int company_id { get; set; }
            public string streetname { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string PIN_code { get; set; }
            public int? processtype { get; set; }
            public string address1 { get; set; }
        }
        public class UrcrmportalupdateaddressdetailsinfoOutput:ErrorMessageModel
        {
        }
    }
}
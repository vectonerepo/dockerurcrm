﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetuserbydeptModel
    {
        public class urcrmportalgetuserbydeptInput
        {
            public int? dept_id { get; set; }

        }
        public class urcrmportalgetuserbydeptOutput : ErrorMessageModel
        {
            public int? userid { get; set; }
            public string username { get; set; }
            public string email { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetticketcategoryModel
    {

        public class urcrmportalgetticketcategoryInput
        {
            public int? category_id { get; set; }

        }
        public class urcrmportalgetticketcategoryOutput : ErrorMessageModel
        {
            public int? category_id { get; set; }
            public string category_name { get; set; }

        }
    }
}
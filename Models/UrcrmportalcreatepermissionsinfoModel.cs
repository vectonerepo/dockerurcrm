﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreatepermissionsinfoModel
    {
        public class UrcrmportalcreatepermissionsinfoInput
        {
            public int? permission_id { get; set; }
            public string permission_name { get; set; }
            public int? permission_status { get; set; }
            public int? processtype { get; set; }
            public string permission_link { get; set; }

        }     
        public class UrcrmportalcreatepermissionsinfoOutput:ErrorMessageModel
        {
        }
    }
}
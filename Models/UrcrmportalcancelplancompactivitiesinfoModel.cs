﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcancelplancompactivitiesinfoModel
    {
        public class UrcrmportalcancelplancompactivitiesinfoInput
        {
            public int company_id { get; set; }
            public int userid { get; set; }
        }
        public class UrcrmportalcancelplancompactivitiesinfoOutput : ErrorMessageModel
        {

        }
    }
}
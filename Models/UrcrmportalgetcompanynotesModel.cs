﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanynotesModel
    {
        public class UrcrmportalgetcompanynotesInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmportalgetcompanynotesOutput:ErrorMessageModel
        {
            public int? Notes_id { get; set; }
            public int? Company_id { get; set; }
            public string Notes_subject { get; set; }
            public string Notes_description { get; set; }
            public DateTime? created_date { get; set; }
        }
    }
}
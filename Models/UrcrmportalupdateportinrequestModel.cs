﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdateportinrequestModel
    {
        public class UrcrmportalupdateportinrequestInput
        {
            public int company_id { get; set; }
            public int reqid { get; set; }
            public int portin_status { get; set; }
            public string comments { get; set; }
        }
        public class UrcrmportalupdateportinrequestOutput:ErrorMessageModel
        {
        }
    }
}
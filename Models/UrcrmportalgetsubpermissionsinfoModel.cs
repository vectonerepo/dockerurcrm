﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetsubpermissionsinfoModel
    {
        public class UrcrmportalgetsubpermissionsinfoInput
        {
            public int sub_perm_id { get; set; }
        }
        public class UrcrmportalgetsubpermissionsinfoOutput:ErrorMessageModel
        {
            public int? sub_perm_id { get; set; }
            public int? fk_sub_perm_id { get; set; }
            public string sub_perm_name { get; set; }
            public int? sub_perm_status { get; set; }
            public string sub_perm_link { get; set; }
            public string permission_name { get; set; }
            public string  menu_type { get; set; }
        }

    }
}
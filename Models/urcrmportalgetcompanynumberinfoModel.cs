﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetcompanynumberinfoModel
    {


        public class urcrmportalgetcompanynumberinfoInput
        {
            public int company_id { get; set; }
            public string  external_number { get; set; }            
            public int processtype { get; set; }
            public string id { get; set; } 
        }
        public class urcrmportalgetcompanynumberinfoOutput : ErrorMessageModel
        {
            public string External_number { get; set; }
            public string number_type { get; set; }
            public DateTime? purchased_date { get; set; }
            public double? price { get; set; }
            public string purchased_by { get; set; }
            public string Assigned_to { get; set; }
            public int? order_id { get; set; }
            public string  upload_path { get; set; }
            public int? main_switchboard { get; set; }
            public string assign_type { get; set; }
            public int? Hubspot_contactid { get; set; }

        }
        
    }
}
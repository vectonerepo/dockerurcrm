﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrminsertplanchangerequestModel
    {
        public class UrcrminsertplanchangerequestInput
        {
            public int company_id { get; set; }
            public int product_id { get; set; }
            public int plan_id { get; set; }
            public int plan_duration { get; set; }
            public int bundle_id { get; set; }
            public double plan_price { get; set; }
            public string reference_no { get; set; }
            public string plan_description { get; set; }
            public double adjustment_charge { get; set; }
            public double tax_fee { get; set; }
            public int tax_per { get; set; }
            public double total_plan_charge { get; set; }
            public int userid { get; set; }
        }
        public class UrcrminsertplanchangerequestOutput:ErrorMessageModel
        {

        }
    }
}
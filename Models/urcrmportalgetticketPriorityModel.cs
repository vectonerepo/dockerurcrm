﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetticketPriorityModel
    {
        public class urcrmportalgetticketPriorityOutput : ErrorMessageModel
        {
            public int? Priority_id { get; set; }
            public string Priority { get; set; }

        }

    }
}
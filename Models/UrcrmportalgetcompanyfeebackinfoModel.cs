﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyfeebackinfoModel
    {
        public class UrcrmportalgetcompanyfeebackinfoInput
        {
            public int company_id { get; set; }
        }


        public class UrcrmportalgetcompanyfeebackinfoOutput:ErrorMessageModel
        {
            public int? company_id { get; set; }
            public int? company_rating { get; set; }
            public string feedback_title { get; set; }
            public string feedback_review { get; set; }
            public DateTime? createddate { get; set; }
            public string called_by { get; set; }
            public string submitted_by { get; set; }          
        }
    }
}
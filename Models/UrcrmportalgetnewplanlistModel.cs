﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetnewplanlistModel
    {
        public class UrcrmportalgetnewplanlistOutput : ErrorMessageModel
        {
            public int? Pln_Idx { get; set; }
            public string New_plan { get; set; }
            public string Pln_Name { get; set; }
            public double? pc_price { get; set; }
            public int? pc_duration { get; set; }
            public string yearly_monthly_Duration { get; set; }
        }
    }
}
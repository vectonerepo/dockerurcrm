﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyuserdetailsinfoModel
    {
        public class UrcrmportalgetcompanyuserdetailsinfoInput
        {
            public int? company_id { get; set; }
            public string extension { get; set; }
            public string User_id { get; set; }
            public int? processtype { get; set; }
        }
        public class UrcrmportalgetcompanyuserdetailsinfoOutput :ErrorMessageModel
        {
            public string user { get; set; }
            public string user_type { get; set; }
            public DateTime? created_on { get; set; }
            public string created_by { get; set; }
            public string ext_no { get; set; }
            public string did_number { get; set; }
            public string location { get; set; }
            public string status { get; set; }
            public string  surname { get; set; }
            public int?  userid { get; set; }
            public int? assign { get; set; }
            public string Role_name { get; set; }
            public string main_number { get; set; }
            public string conference_number { get; set; }
            public string  assigned_phone_number { get; set; }
        }
    }
}
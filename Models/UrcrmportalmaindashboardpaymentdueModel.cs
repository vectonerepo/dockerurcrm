﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalmaindashboardpaymentdueModel
    {
        public class UrcrmportalmaindashboardpaymentdueInput
        {
            public int processtype { get; set; }
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        public class UrcrmportalmaindashboardpaymentdueOutput : ErrorMessageModel
        {
            public double? Tot_payment_due { get; set; }
            public double? new_payment_due { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetpermissiontorolesModel
    {
        public class UrcrmportalgetpermissiontorolesInput
        {
            public int Role_id { get; set; }
        }
        public class UrcrmportalgetpermissiontorolesOutput:ErrorMessageModel
        {
            
            public string Role_name { get; set; }
            public int? sub_perm_id { get; set; }
            public string sub_perm_name { get; set; }
            public string description { get; set; }
        }
    }
}
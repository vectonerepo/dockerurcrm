﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdatecompanyuserdetailsinfoModel
    {
        public class UrcrmportalupdatecompanyuserdetailsinfoInput
        {
            public int company_id { get; set; }
            public string extension_Number { get; set; }
            public string user_firstname { get; set; }
            public int status { get; set; }
            public string user_Surname { get; set; }
        }
        public class UrcrmportalupdatecompanyuserdetailsinfoOutput:ErrorMessageModel
        {
           
        }
    }
}
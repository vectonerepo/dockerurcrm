﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportaluserloginModel
    {
        public class urcrmportaluserloginInput
        {
            public string username { get; set; }
            public string password { get; set; }
            
        }
        public class urcrmportaluserloginOutput : ErrorMessageModel
        {
            public int user_role { get; set; }
            public int userid { get; set; }
            public string  username { get; set; }
        }
    }
}
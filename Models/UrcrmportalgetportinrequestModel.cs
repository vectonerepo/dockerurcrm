﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetportinrequestModel
    {
        public class UrcrmportalgetportinrequestInput
        {
            public int company_id { get; set; }
            public int reqid { get; set; }
        }
        public class UrcrmportalgetportinrequestOutput:ErrorMessageModel
        {
            public string order_number { get; set; }
            public string req_from { get; set; }
            public DateTime? create_date { get; set; }
            public string req_to { get; set; }
            public string number_type { get; set; }
            public string service_provider { get; set; }
            public int? status_id { get; set; }
            public string status { get; set; }
            public string cupid_id { get; set; }
            public string signedLOA_path { get; set; }
            public string upload_path { get; set; }
            public DateTime? transfer_date { get; set; }
            public string assigned_to { get; set; }
            public string transfer_no { get; set; }
            public string replace_no { get; set; }
            public int? reqid { get; set; }
            public string comments { get; set; }
        }
    }
}
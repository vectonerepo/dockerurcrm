﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetuserroleinfoModel
    {
        public class UrcrmportalgetuserroleinfoInput
        {
            public int user_id { get; set; }
        }
        public class UrcrmportalgetuserroleinfoOutput:ErrorMessageModel
        {
            public int? userid { get; set; }
            public string username { get; set; }
            public string Role_name { get; set; }
            public int? user_role { get; set; }
            public int? status { get; set; }
            public string password { get; set; }
            public DateTime? createdate { get; set; }
        }

    }
}
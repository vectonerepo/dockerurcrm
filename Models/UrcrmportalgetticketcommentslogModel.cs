﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetticketcommentslogModel
    {
        public class UrcrmportalgetticketcommentslogInput
        {
            public int Ticket_id { get; set; }
        }
        public class UrcrmportalgetticketcommentslogOutput:ErrorMessageModel
        {
            public int? id { get; set; }
            public int? Ticket_id { get; set; }
            public string comments { get; set; }
            public DateTime? create_date { get; set; }
        }
    }
}
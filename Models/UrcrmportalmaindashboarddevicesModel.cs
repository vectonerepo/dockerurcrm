﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalmaindashboarddevicesModel
    {
        public class UrcrmportalmaindashboarddevicesInput
        {
            public int processtype { get; set; }
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        public class UrcrmportalmaindashboarddevicesOutput : ErrorMessageModel
        {
            public int? Total_devices { get; set; }
            public int? New_devices { get; set; }
        }
    }
}
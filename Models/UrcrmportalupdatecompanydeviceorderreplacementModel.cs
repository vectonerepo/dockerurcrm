﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdatecompanydeviceorderreplacementModel
    {
        public class UrcrmportalupdatecompanydeviceorderreplacementInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
            public int device_approval { get; set; }
            public string tracking_no { get; set; }
            public string delivery_comment { get; set; }
            public DateTime? delivery_date { get; set; }
            public string comments { get; set; }
        }
        public class UrcrmportalupdatecompanydeviceorderreplacementOutput:ErrorMessageModel
        {

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalmaindashboardnumbersModel
    {
        public class UrcrmportalmaindashboardnumbersInput
        {
            public int processtype { get; set; }
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        public class UrcrmportalmaindashboardnumbersOutput : ErrorMessageModel
        {
            public int? Total_numbers { get; set; }
            public int? New_numbers { get; set; }
        }
    }
}
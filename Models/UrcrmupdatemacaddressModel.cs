﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmupdatemacaddressModel
    {
        public class UrcrmupdatemacaddressInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
            public string  mac_address { get; set; }
            public int referenceid { get; set; }
        }
        public class UrcrmupdatemacaddressOutput:ErrorMessageModel
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalmaindashboardcompaniesModel
    {
        public class UrcrmportalmaindashboardcompaniesInput
        {
            public int processtype { get; set; }
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        public class UrcrmportalmaindashboardcompaniesOutput : ErrorMessageModel
        {
            public int? Total_users { get; set; }
            public int? New_users { get; set; }
        }
    }
}
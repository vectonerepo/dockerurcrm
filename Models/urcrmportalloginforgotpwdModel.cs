﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalloginforgotpwdModel
    {
        public class urcrmportalloginforgotpwdInput
        {
            public string email { get; set; }
            public string new_pwd { get; set; }
            public int type { get; set; }
            public int log_id { get; set; }
        }
        public class urcrmportalloginforgotpwdOutput : ErrorMessageModel
        {
            public string first_name { get; set; }
            public string pass_word { get; set; }
            public int? userid { get; set; }
            public int? log_id { get; set; }
            public string email { get; set; }
        }
    }
}
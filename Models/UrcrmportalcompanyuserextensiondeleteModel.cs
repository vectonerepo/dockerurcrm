﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcompanyuserextensiondeleteModel
    {

        public class UrcrmportalcompanyuserextensiondeleteInput
        {
            public int company_id { get; set; }
            public string  extension { get; set; }
        }
        public class UrcrmportalcompanyuserextensiondeleteOutput:ErrorMessageModel
        {
        }
    }
}
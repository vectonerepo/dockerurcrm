﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetticketsModel
    {

        public class UrcrmportalgetticketsInput
        {
            public int? Ticket_id { get; set; }
        }
        public class UrcrmportalgetticketsOutput:ErrorMessageModel
        {
            public string Ticket_id { get; set; }
            public int? id { get; set; }
            public string Title { get; set; }
            public string desc { get; set; }
            public int? category_id { get; set; }
            public string category_name { get; set; }
            public int? Sub_category_id { get; set; }
            public string sub_category_name { get; set; }
            public int? Fk_Status { get; set; }
            public string Status_name { get; set; }
            public int? Fk_priorty_id { get; set; }
            public string Priority { get; set; }
            public DateTime? created_on { get; set; }
            public string create_by { get; set; }
            public int? dept_id { get; set; }
            public string dept_name { get; set; }
            public string attachments { get; set; }
            public int? userid { get; set; }
            public string username { get; set; }
            public string company_name { get; set; }
            public int? company_id { get; set; }
            public string comments { get; set; }
            public DateTime? modify_date { get; set; }
        }
    }
}
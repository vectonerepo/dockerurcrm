﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetorderslistfailedModel
    {
        public class UrcrmportalgetorderslistfailedInput
        {
        }
        public class UrcrmportalgetorderslistfailedOutput : ErrorMessageModel
        {
            public string mobileno { get; set; }
            public string REFERENCE_ID { get; set; }
            public string Pay_decision { get; set; }
            public string Pay_description { get; set; }
            public DateTime? created_date { get; set; }
        }
    }
}
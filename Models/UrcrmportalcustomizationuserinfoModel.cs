﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcustomizationuserinfoModel
    {
        public class UrcrmportalcustomizationuserinfoInput
        {
            public int company_id { get; set; }
            public int processtype { get; set; }
            public string ID { get; set; }
        }
        public class UrcrmportalcustomizationuserinfoOutput:ErrorMessageModel
        {
            public string username { get; set; }
            public string number { get; set; }
            public string Extension_Number { get; set; }
            public string Roles { get; set; }
            public string company { get; set; }
            public string Department { get; set; }
            public string Status { get; set; }
            public int? ID { get; set; }
        }
    }
}
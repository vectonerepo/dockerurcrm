﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportaldeleteportinrequestModel
    {
        public class urcrmportaldeleteportinrequestInput
        {
            public int company_id { get; set; }
            public string id { get; set; }
        }
        public class urcrmportaldeleteportinrequestOutput : ErrorMessageModel
        {

        }
    }
}
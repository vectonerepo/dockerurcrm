﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetbundlelistinfoModel
    {
        public class UrcrmportalgetbundlelistinfoInput
        {
            public int bundleid { get; set; }
        }
        public class UrcrmportalgetbundlelistinfoOutput:ErrorMessageModel
        {
            public int? id { get; set; }
            public int? bundleid { get; set; }
            public string bundle_name { get; set; }
            public double? price { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int? bundle_status { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreateticketModel
    {
        public class UrcrmportalcreateticketInput
        {
            public string Title { get; set; }
            public string desc { get; set; }
            public int? Fk_category_id { get; set; }
            public int? Fk_Sub_category_id { get; set; }
            public int? Fk_Status { get; set; }
            public int? Fk_priorty_id { get; set; }
            public int? Fk_User_id { get; set; }
            public string create_by { get; set; }
            public string attachments { get; set; }
            public int? process_type { get; set; }
            public int? Ticket_id { get; set; }
            public int? company_id { get; set; }
            public string comments { get; set; }
            //public int Ticket_id { get; set; }
            //public int company_id { get; set; }
            //public string comments { get; set; }
        }

          //--Ticket_id INT = 0, company_id int  =0, comments longtext =’’


        public class UrcrmportalcreateticketOutput : ErrorMessageModel
        {
            public string Ticket_id { get; set; }
        }


    }
}
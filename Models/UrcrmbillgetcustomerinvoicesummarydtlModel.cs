﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmbillgetcustomerinvoicesummarydtlModel
    {
        public class UrcrmbillgetcustomerinvoicesummarydtlInput
        {
            public int company_id { get; set; }
            public int invoice_id { get; set; }
        }
        public class UrcrmbillgetcustomerinvoicesummarydtlOutput:ErrorMessageModel
        {
            public string charge_type { get; set; }
            public string charge_number { get; set; }
            public double? charge { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmemergcontactgetModel
    {

        public class UrcrmemergcontactgetInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmemergcontactgetOutput:ErrorMessageModel
        {
            public string post_code { get; set; }
            public string address_line_1 { get; set; }
            public string address_line_2 { get; set; }
            public string address_line_3 { get; set; }
            public string city { get; set; }
        }

    }
}
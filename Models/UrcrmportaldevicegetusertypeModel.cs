﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportaldevicegetusertypeModel
    {
        public class UrcrmportaldevicegetusertypeInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmportaldevicegetusertypeOutput:ErrorMessageModel
        {
            public int? user_type { get; set; }
            public int? tot_count { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetportinstatusdrpModel
    {
        public class UrcrmportalgetportinstatusdrpInput
        {
        }
        public class UrcrmportalgetportinstatusdrpOutput:ErrorMessageModel
        {
            public int status_id { get; set; }
            public string status_desc { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalmaindashboardplanstatusModel
    {
        public class UrcrmportalmaindashboardplanstatusInput
        {
            public int processtype { get; set; }
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        
        public class UrcrmportalmaindashboardplanstatusOutput : ErrorMessageModel
        {
            public List<PlanDetails> PlanDetails { get; set; }
            public List<DateDetails> DateDetails { get; set; }          
        }

        public class PlanDetails
        {
            public int? plan_upgrade { get; set; }
            public int? plan_downgrade { get; set; }
            public int? plan_cancel { get; set; }
        }

        public class DateDetails
        {
            public DateTime? Date { get; set; }
            public int? upgrade { get; set; }
            public int? downgrade { get; set; }
            public int? cancel { get; set; }
        }
    }
}
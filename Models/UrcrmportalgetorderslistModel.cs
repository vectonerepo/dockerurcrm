﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetorderslistModel
    {
        public class UrcrmportalgetorderslistOutput : ErrorMessageModel
        {
            public int? customer_id { get; set; }
            public string company_name { get; set; }
            public int? Orderid { get; set; }
            public DateTime? Subscription_date { get; set; }
            public string Subscription_type { get; set; }
            public string created_by { get; set; }
            public string order_type { get; set; }
            public string plan_name { get; set; }
            public int? no_of_users { get; set; }
            public double? Amount_paid { get; set; }
            public string Payment_ref { get; set; }
            public string status { get; set; }
        }
        public class UrcrmportalgetorderslistInput
        {
            public int company_id { get; set; }
            public string Subscription_type { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreateticketcommentslogModel
    {
        public class UrcrmportalcreateticketcommentslogInput
        {
            public int Ticket_id { get; set; }
            public string comments { get; set; }
        }
        public class UrcrmportalcreateticketcommentslogOutput:ErrorMessageModel
        {
        }
    }
}
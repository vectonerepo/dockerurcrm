﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreateuserroleinfoModel
    {
        public class UrcrmportalcreateuserroleinfoInput
        {
            public string username_email { get; set; }
            public string password { get; set; }
            public int user_role_id { get; set; }
            public int user_status { get; set; }
            public int user_id { get; set; }
            public int dept_id { get; set; }
            public int processtype { get; set; }
        }
        public class UrcrmportalcreateuserroleinfoOutput:ErrorMessageModel
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreatecompanyemailinfoModel
    {
        public class UrcrmportalcreatecompanyemailinfoInput
        {
            public  int email_id { get; set; }
            public int? company_id { get; set; }
            public string email_from { get; set; }
            public string email_to { get; set; }
            public string email_subject { get; set; }
            public string email_body { get; set; }
            public int? no_of_attachaments { get; set; }
            public string attachment_location { get; set; }
            public string create_by { get; set; }
            public int processtype { get; set; }
        }
        public class UrcrmportalcreatecompanyemailinfoOutput:ErrorMessageModel
        {
            
        }
    }
}
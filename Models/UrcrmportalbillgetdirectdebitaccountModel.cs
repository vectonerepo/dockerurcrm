﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalbillgetdirectdebitaccountModel
    {
        public class UrcrmportalbillgetdirectdebitaccountInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmportalbillgetdirectdebitaccountOutput:ErrorMessageModel
        {
            public string firstname { get; set; }
            public string surname { get; set; }
            public string mobileno { get; set; }
            public string email { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public string card_type { get; set; }
            public string expirydate { get; set; }
            public string cc_no { get; set; }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyemailinfoModel
    {
        public class UrcrmportalgetcompanyemailinfoInput
        {
            public int company_id { get; set; }
            public int emailtype { get; set; }
            public int processtype { get; set; }
            public string id { get; set; }
        }
        public class UrcrmportalgetcompanyemailinfoOutput:ErrorMessageModel
        {
            public string email_from { get; set; }
            public string email_to { get; set; }
            public string email_subject { get; set; }
            public DateTime? create_date { get; set; }
            public int email_id { get; set; }
            public string attachment_location { get; set; }
            public string email_body { get; set; }
            public DateTime? modify_date { get; set; }
        }
    }
}
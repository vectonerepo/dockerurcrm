﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmgetmacaddressModel
    {
        public class UrcrmgetmacaddressInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
            public string referenceid { get; set; }
        }
        public class UrcrmgetmacaddressOutput:ErrorMessageModel
        {
            public int? dir_users_id { get; set; }
            public string MAC_address { get; set; }          
        }
    }
}
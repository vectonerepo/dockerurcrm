﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetnewplaninfoModel
    {
        public class urcrmportalgetnewplaninfoInput
        {
            public int company_id { get; set; }
            public int plan_duration_input { get; set; }
            public int no_of_user { get; set; }
        }

       // ur_crm_portal_get_new_plan_info

        public class urcrmportalgetnewplaninfoOutput : ErrorMessageModel
        {
            public int? Pln_Idx { get; set; }
            public string New_plan { get; set; }
            public string Pln_Name { get; set; }
            public string pc_price { get; set; }
            public int? pc_duration { get; set; }
            public string yearly_monthly_Duration { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyorderinfoModel
    {
        public class UrcrmportalgetcompanynumberinfoInput
        {
            public int invoice_id { get; set; }
            public int company_id { get; set; }
        }
        public class UrcrmportalgetcompanyorderinfoOutput : ErrorMessageModel
        {
            public int? invoice_id { get; set; }
            public DateTime? bill_date { get; set; }
            public string bill_duration { get; set; }
            public DateTime? bill_due_date { get; set; }
            public string reference_no { get; set; }
            public string description { get; set; }
            public double? charge { get; set; }
            public double? adjustment { get; set; }
            public double? tax_percentage { get; set; }
            public double? tax_fee { get; set; }
            public double? total_bill { get; set; }
            public double? total_paid { get; set; }
            public string pay_type { get; set; }
            public DateTime? payment_date { get; set; }
            public string payment_reference { get; set; }
            public string comp_name { get; set; }
            public string comp_regno { get; set; }
            public string comp_address { get; set; }
            public string comp_building { get; set; }
            public string comp_street { get; set; }
            public string comp_city { get; set; }
            public string comp_country { get; set; }
            public string service_plan { get; set; }
            public double? total_delivery_fee { get; set; }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalcompanydetailsinfoModel
    {
        public class urcrmportalcompanydetailsinfoInput
        {
            public int? company_id { get; set; }

        }
        public class urcrmportalcompanydetailsinfoOutput : ErrorMessageModel
        {
            public int? company_id { get; set; }
            public string company_name { get; set; }
            public string Account_status { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string city { get; set; }
            public string contact_first_name { get; set; }
            public string contact_last_name { get; set; }
            public string  contact_phone { get; set; }
            public string  email { get; set; }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetticketstatusModel
    {

        public class urcrmportalgetticketstatusOutput : ErrorMessageModel
        {
            public int? Status_id { get; set; }
            public string Status_name { get; set; }
        }
    }
}
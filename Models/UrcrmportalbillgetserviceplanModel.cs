﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalbillgetserviceplanModel
    {

        public class UrcrmportalbillgetserviceplanInput
        {
            public int company_id { get; set; }
        }
        public class UrcrmportalbillgetserviceplanOutput:ErrorMessageModel
        {
            public string prd_name { get; set; }
            public string pln_name { get; set; }
            public double? pc_price { get; set; }
            public string price_per_user { get; set; }
            public string ord_user_range { get; set; }
            public double? account_credit { get; set; }
            public string contract_period { get; set; }
            public string bill_cycle { get; set; }
            public string bill_date { get; set; }
            public string bill_due_date { get; set; }
            public string bundle_plan { get; set; }
            public double? bundle_mins_usage { get; set; }
            public int? tot_localno { get; set; }
            public double? localno_price { get; set; }
            public int? tot_inter_nat_no { get; set; }
            public double? inter_nat_no_price { get; set; }
            public int? tot_non_geo_no { get; set; }
            public double? non_geo_no_price { get; set; }
            public string bill_cycle_type { get; set; }
            public double? calling_credit { get; set; }
            public double? threshold_limit { get; set; }
            public int? Is_free_trail { get; set; }
            public string new_price_combined { get; set; }
            public double? Calling_UK_mins { get; set; }
            public int? device_phone_count { get; set; }
            public int? Unlimited_inbound_conf { get; set; }
            public double? Unlimited_inbound_conf_value { get; set; }           
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreatesubpermissionsinfoModel
    {
        public class UrcrmportalcreatesubpermissionsinfoInput
        {
            public int sub_id { get; set; }
            public int fk_sub_perm_id { get; set; }
            public string sub_perm_name { get; set; }
            public int sub_perm_status { get; set; }
            public int processtype { get; set; }
            public string  sub_perm_link { get; set; }
            public string menu_type { get; set; }
        }
        public class UrcrmportalcreatesubpermissionsinfoOutput:ErrorMessageModel
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalvalidatebundlesubscribeModel
    {
        public class UrcrmportalvalidatebundlesubscribeInput
        {
            public int company_id { get; set; }
            public int bundle_id { get; set; }
        }
        public class UrcrmportalvalidatebundlesubscribeOutput:ErrorMessageModel
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetconfigurationrolesModel
    {
        public class UrcrmportalgetconfigurationrolesInput 
        {
            public int role_id { get; set; }
            
        }
        public class UrcrmportalgetconfigurationrolesOutput:ErrorMessageModel
        {
            public string Role_name { get; set; }
            public string Role_type { get; set; }
            public string Description { get; set; }
            public int? Role_status { get; set; }
            public int? Role_id { get; set; }
            public string Roles_permission { get; set; }
        }
    }
}
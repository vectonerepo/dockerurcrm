﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanydashboardinfoModel
    {
        public class UrcrmportalgetcompanydashboardinfoInput
        {
            public int company_id { get; set; }
        }

        public class UrcrmportalgetcompanydashboardinfoOutput : ErrorMessageModel
        {
            public List<companyDetails> companyDetails { get; set; }
            public List<UserDetails> UserDetails { get; set; }
            public List<DidDeatils> DidDeatils { get; set; }           
            public List<Countrydeatils> Countrydeatils { get; set; }
        }

        public class companyDetails
        {
            public string company_name { get; set; }
            public string plan_name { get; set; }
            public DateTime? payment_due_date { get; set; }
            public string Subscription_type { get; set; }
            public DateTime? plan_Subscription_date { get; set; }
            //public DateTime? billing_cycle { get; set; }
            public string billing_cycle { get; set; }
            public long? Hubspot_company_id { get; set; }
        }

        public class UserDetails
        {
            public int? Total_users { get; set; }
            public int? Total_numbers { get; set; }
            public int? Total_device { get; set; }
            public DateTime? end_date { get; set; }
            public double? initial_credit { get; set; }
            public double? available_credit { get; set; }
            public double? freemin { get; set; }
            public double? Total_payment_collected { get; set; }
            public double? payment_due { get; set; }
            public int? Total_tickets { get; set; }
            public int? Tickets_open { get; set; }
        }

       
        public class DidDeatils
        {
            public int? user_type { get; set; }
            public int? tot_count { get; set; }
            //public int? Did_users { get; set; }
            //public int? Non_Did_users { get; set; }
        }
       
        public class Countrydeatils
        {
            public string country_name { get; set; }
            public int? numbers_purchased { get; set; }
        }
    }
}
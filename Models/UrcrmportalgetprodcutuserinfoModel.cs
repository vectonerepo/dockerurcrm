﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetprodcutuserinfoModel
    {
        public class UrcrmportalgetprodcutuserinfoInput
        {
            public int company_id { get; set; }
            public int search_mode { get; set; }
        }
        public class UrcrmportalgetprodcutuserinfoOutput:ErrorMessageModel
        {
            public string Firstname { get; set; }
            public string Email { get; set; }
            public string device_type { get; set; }
            public string browser { get; set; }
        }
    }
}
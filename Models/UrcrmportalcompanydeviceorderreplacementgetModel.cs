﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcompanydeviceorderreplacementgetModel
    {
        public class UrcrmportalcompanydeviceorderreplacementgetInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
        }
        public class UrcrmportalcompanydeviceorderreplacementgetOutput:ErrorMessageModel
        {
            public int? id { get; set; }
            public int? order_id { get; set; }
            public string prod_code { get; set; }
            public string prod_name { get; set; }
            public string prod_model { get; set; }
            public DateTime? rpl_date { get; set; }
            public int? rpl_quantity { get; set; }
            public string rpl_reason { get; set; }
            public int? rpl_status { get; set; }
            public string tracking_no { get; set; }
            public string comments { get; set; }
            public int? company_id { get; set; }
            public string company_name { get; set; }
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
            public DateTime? order_date { get; set; }
            public string referenceid { get; set; }
            public string  status_desc { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetaddressdetailsinfoModel
    {
        public class UrcrmportalgetaddressdetailsinfoInput
        {
            public int company_id { get; set; }
            public int type { get; set; }

        }
        public class UrcrmportalgetaddressdetailsinfoOutput : ErrorMessageModel
        {
            public string streetname { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string PIN_code { get; set; }
            public string user_name { get; set; }
            public string Email_address { get; set; }
            public string Password { get; set; }
            public string  address1 { get; set; }
        }
    }
}
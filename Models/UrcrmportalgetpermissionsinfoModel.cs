﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetpermissionsinfoModel
    {
        public class UrcrmportalgetpermissionsinfoInput
        {
            public int permission_id { get; set; }

        }
        public class UrcrmportalgetpermissionsinfoOutput : ErrorMessageModel
        {

            public int? permission_id { get; set; }
            public string permission_name { get; set; }
            public int? permission_status { get; set; }
            public string permission_link { get; set; }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyactivitiesdtlModel
    {
        public class UrcrmportalgetcompanyactivitiesdtlInput
        {
            public int company_id { get; set; }
            public int userid { get; set; }
        }
        public class UrcrmportalgetcompanyactivitiesdtlOutput:ErrorMessageModel
        {
            public DateTime? createdate { get; set; }
            public string name { get; set; }
            public string activity { get; set; }
            public string type { get; set; }
            public string status { get; set; }
            public string username { get; set; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreateconfigurationrolesModel
    {
        public class UrcrmportalcreateconfigurationrolesInput
        {
            public int Processtype { get; set; }
            public string Role_name { get; set; }
            public string Description { get; set; }
            public int Role_status { get; set; }
            public int Role_id { get; set; }
            public string features { get; set; }
        }
        public class UrcrmportalcreateconfigurationrolesOutput:ErrorMessageModel
        {
           
        }
    }
}
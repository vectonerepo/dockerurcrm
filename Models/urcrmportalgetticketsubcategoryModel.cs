﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetticketsubcategoryModel
    {
        public class urcrmportalgetticketsubcategoryInput
        {
            public int? category_id { get; set; }
            public int? sub_category_id { get; set; }

        }
        public class urcrmportalgetticketsubcategoryOutput : ErrorMessageModel
        {
            public int? Sub_category_id { get; set; }
            public string sub_category_name { get; set; }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmRoleMenuMappinggetinfoModel
    {
        public class UrcrmRoleMenuMappinggetinfoInput
        {
            public int Role_ID { get; set; }
            public int sub_perm_id { get; set; }
        }
        public class UrcrmRoleMenuMappinggetinfoOutput:ErrorMessageModel
        {
        }
    }
}


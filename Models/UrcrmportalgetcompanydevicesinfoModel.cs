﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanydevicesinfoModel
    {
        public class UrcrmportalgetcompanydevicesinfoInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
        }
        public class UrcrmportalgetcompanydevicesinfoOutput:ErrorMessageModel
        {
            public int? order_id { get; set; }
            public string model_name { get; set; }
            public string model_number { get; set; }
            public double? price { get; set; }
            public string order_placed_by { get; set; }
            public int? order_status { get; set; }
            public string mac_id { get; set; }
            public DateTime? Ordered_On { get; set; }
            public DateTime? last_update { get; set; }
            public string category { get; set; }
            public string referenceid { get; set; }
            public string tracking_no { get; set; }
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
            public string comment { get; set; }
        }
    }
}
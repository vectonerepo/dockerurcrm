﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalgetcompanyactivitiesinfoModel
    {
        public class urcrmportalgetcompanyactivitiesinfoInput
        {
            public int? company_id { get; set; }

        }
        public class urcrmportalgetcompanyactivitiesinfoOutput : ErrorMessageModel
        {
            public int? cp_id { get; set; }
            public DateTime? plan_Subscription_date { get; set; }
            public string plan_changes { get; set; }
            public string billing_cycle_changes { get; set; }
            public DateTime? minutes_usages { get; set; }
            public double? extra_minutes_usages { get; set; }
            public double? international_calling { get; set; }
            public double? bundle_subcription { get; set; }
            public string  status { get; set; }
            public DateTime? Activity_time { get; set; }
            public string  activity_done_by { get; set; }
            public string Activity { get; set; }
            public string bundle_name { get; set; }
        }
    }
}
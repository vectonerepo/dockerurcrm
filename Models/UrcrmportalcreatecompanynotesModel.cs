﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalcreatecompanynotesModel
    {
        public class UrcrmportalcreatecompanynotesInput
        {
            public int? company_id { get; set; }
            public string notes_subject { get; set; }
            public string Notes_description { get; set; }
            public int? processtype { get; set; }
            public string notes_id { get; set; }
        }
        public class UrcrmportalcreatecompanynotesOutput:ErrorMessageModel
        {
            
        }
    }
}
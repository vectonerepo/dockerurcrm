﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalgetcompanyinvoiceModel
    {
        public class UrcrmportalgetcompanyinvoiceInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
        }
        public class UrcrmportalgetcompanyinvoiceOutput:ErrorMessageModel
        {
            public int order_id { get; set; }
            public string referenceid { get; set; }
            public double? tot_phone_charge { get; set; }
            public double? tot_discount { get; set; }
            public double? tot_tax_fee { get; set; }
            public double? tot_charge { get; set; }
            public DateTime? createdate { get; set; }
            public string order_source { get; set; }
            public DateTime? approval_date { get; set; }
            public string tracking_no { get; set; }
            public DateTime? delivery_date { get; set; }
            public string duration_period { get; set; }
            public double? duration_charge { get; set; }
            public double? total_delivery_fee { get; set; }
            public string adjustment_period { get; set; }
            public double? adjustment_charge { get; set; }
            public double? vat_percetage { get; set; }
            public double? vat_charge { get; set; }
            public double? sub_total { get; set; }
            public string category { get; set; }
            public string phone_id { get; set; }
            public string phone_model { get; set; }
            public string phone_desc { get; set; }
            public double? weight { get; set; }
            public int quantity { get; set; }
            public int order_with_number { get; set; }
            public double? order_with_number_price { get; set; }
        }
    }
}
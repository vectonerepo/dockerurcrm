﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdatecompanydevicesinfoModel
    {
        public class UrcrmportalupdatecompanydevicesinfoInput
        {
            public int company_id { get; set; }
            public int order_id { get; set; }
            public int device_approval { get; set; }
            public string mac_address { get; set; }
            public string category { get; set; }
            public string tracking_no { get; set; }
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
            public string comment { get; set; }
        }
        public class UrcrmportalupdatecompanydevicesinfoOutput:ErrorMessageModel
        {
        }
    }
}
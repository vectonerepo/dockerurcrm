﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmbillingaddressupdateModel
    {
        public class UrcrmbillingaddressupdateInput
        {
            public int company_id { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public int process_type { get; set; }
        }
        public class UrcrmbillingaddressupdateOutput:ErrorMessageModel
        {
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
        }
    }
}
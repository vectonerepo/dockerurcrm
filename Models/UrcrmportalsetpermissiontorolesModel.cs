﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalsetpermissiontorolesModel
    {

        public class UrcrmportalsetpermissiontorolesInput
        {
            public int Role_id { get; set; }
            public string features { get; set; }
        }
        public class UrcrmportalsetpermissiontorolesOutput:ErrorMessageModel
        {
        }
    }
}
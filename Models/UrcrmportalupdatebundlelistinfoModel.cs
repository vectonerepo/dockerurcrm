﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class UrcrmportalupdatebundlelistinfoModel
    {
        public class UrcrmportalupdatebundlelistinfoInput
        {
            public int bundleid { get; set; }
            public int bundle_status { get; set; }
            public string bundle_name { get; set; }
            public double price { get; set; }
            public double? national_min { get; set; }
            public double? international_min { get; set; }
            public int Reseller_flag { get; set; }
            public int bundle_type { get; set; }

        }
        public class UrcrmportalupdatebundlelistinfoOutput:ErrorMessageModel
        {

        }

    }
}
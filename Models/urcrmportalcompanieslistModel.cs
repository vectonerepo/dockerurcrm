﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace URCRMDocker.Models
{
    public class urcrmportalcompanieslistModel
    {
        public class urcrmportalcompanieslistInput
        {
            public string From_date { get; set; }
            public string to_date { get; set; }
        }
        public class urcrmportalcompanieslistOutput : ErrorMessageModel
        {
            public int? company_id { get; set; }
            public string comp_name { get; set; }
            public string  Pln_Name { get; set; }
            public string Subscription_type { get; set; }
            public DateTime? Subscription_date { get; set; }
            public string Location { get; set; }
            public string status { get; set; }
            public string email { get; set; }
            public string Comp_is_free_trail { get; set; }
            public DateTime? plan_end_date { get; set; }
            public string create_by { get; set; }
            public string create_from { get; set; }

        }
    }
}
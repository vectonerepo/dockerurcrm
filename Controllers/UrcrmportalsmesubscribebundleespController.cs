﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalsmesubscribebundleespModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalsmesubscribebundleespController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalsmesubscribebundleespController> _logger;

        public UrcrmportalsmesubscribebundleespController(ILogger<UrcrmportalsmesubscribebundleespController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalsmesubscribebundleespOutput>> CreateAsync(UrcrmportalsmesubscribebundleespInput req)
        {
            List<UrcrmportalsmesubscribebundleespOutput> result = new List<UrcrmportalsmesubscribebundleespOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalsmesubscribebundleespDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalsmesubscribebundleespOutput outputobj = new UrcrmportalsmesubscribebundleespOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
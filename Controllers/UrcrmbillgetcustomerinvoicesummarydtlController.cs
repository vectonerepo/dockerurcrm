﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmbillgetcustomerinvoicesummarydtlModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmbillgetcustomerinvoicesummarydtlController : ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<UrcrmbillgetcustomerinvoicesummarydtlController> _logger;

        public UrcrmbillgetcustomerinvoicesummarydtlController(ILogger<UrcrmbillgetcustomerinvoicesummarydtlController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;

     
        }
        [Authorize]
        [HttpPost]
        
        public async Task<ActionResult<UrcrmbillgetcustomerinvoicesummarydtlOutput>> CreateAsync(UrcrmbillgetcustomerinvoicesummarydtlInput req)
        {
            List<UrcrmbillgetcustomerinvoicesummarydtlOutput> result = new List<UrcrmbillgetcustomerinvoicesummarydtlOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmbillgetcustomerinvoicesummarydtlDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {
                UrcrmbillgetcustomerinvoicesummarydtlOutput outputobj = new UrcrmbillgetcustomerinvoicesummarydtlOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                return Ok(outputobj);
            }
        }
    }
}

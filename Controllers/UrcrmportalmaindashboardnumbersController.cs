﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalmaindashboardnumbersModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalmaindashboardnumbersController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalmaindashboardnumbersController> _logger;

        public UrcrmportalmaindashboardnumbersController(ILogger<UrcrmportalmaindashboardnumbersController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalmaindashboardnumbersOutput>> CreateAsync(UrcrmportalmaindashboardnumbersInput req)
        {
            List<UrcrmportalmaindashboardnumbersOutput> result = new List<UrcrmportalmaindashboardnumbersOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalmaindashboardnumbersDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalmaindashboardnumbersOutput outputobj = new UrcrmportalmaindashboardnumbersOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportalupgradeplancompactivitiesinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalupgradeplancompactivitiesinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalupgradeplancompactivitiesinfoController> _logger;

        public urcrmportalupgradeplancompactivitiesinfoController(ILogger<urcrmportalupgradeplancompactivitiesinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalupgradeplancompactivitiesinfoOutput>> CreateAsync(urcrmportalupgradeplancompactivitiesinfoInput req)
        {
            List<urcrmportalupgradeplancompactivitiesinfoOutput> result = new List<urcrmportalupgradeplancompactivitiesinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalupgradeplancompactivitiesinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalupgradeplancompactivitiesinfoOutput outputobj = new urcrmportalupgradeplancompactivitiesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.urcrmportalgetticketdeptModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalgetticketdeptController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetticketdeptController> _logger;

        public urcrmportalgetticketdeptController(ILogger<urcrmportalgetticketdeptController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<urcrmportalgetticketdeptOutput>> CreateAsync()
        {
            List<urcrmportalgetticketdeptOutput> result = new List<urcrmportalgetticketdeptOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetticketdeptDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetticketdeptOutput outputobj = new urcrmportalgetticketdeptOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);         
            }

        }
    }
}
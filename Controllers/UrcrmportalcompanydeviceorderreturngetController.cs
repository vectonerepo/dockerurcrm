﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcompanydeviceorderreturngetModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcompanydeviceorderreturngetController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcompanydeviceorderreturngetController> _logger;

        public UrcrmportalcompanydeviceorderreturngetController(ILogger<UrcrmportalcompanydeviceorderreturngetController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcompanydeviceorderreturngetOutput>> CreateAsync(UrcrmportalcompanydeviceorderreturngetInput req)
        {
            List<UrcrmportalcompanydeviceorderreturngetOutput> result = new List<UrcrmportalcompanydeviceorderreturngetOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcompanydeviceorderreturngetDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcompanydeviceorderreturngetOutput outputobj = new UrcrmportalcompanydeviceorderreturngetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);         
            }
        }
    }
}
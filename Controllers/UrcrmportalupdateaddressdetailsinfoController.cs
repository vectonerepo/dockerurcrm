﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdateaddressdetailsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdateaddressdetailsinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdateaddressdetailsinfoController> _logger;

        public UrcrmportalupdateaddressdetailsinfoController(ILogger<UrcrmportalupdateaddressdetailsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdateaddressdetailsinfoOutput>> CreateAsync(UrcrmportalupdateaddressdetailsinfoInput req)
        {
            List<UrcrmportalupdateaddressdetailsinfoOutput> result = new List<UrcrmportalupdateaddressdetailsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdateaddressdetailsinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdateaddressdetailsinfoOutput outputobj = new UrcrmportalupdateaddressdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
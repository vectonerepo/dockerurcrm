﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdatebundlelistinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdatebundlelistinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdatebundlelistinfoController> _logger;

        public UrcrmportalupdatebundlelistinfoController(ILogger<UrcrmportalupdatebundlelistinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdatebundlelistinfoOutput>> CreateAsync(UrcrmportalupdatebundlelistinfoInput req)
        {
            List<UrcrmportalupdatebundlelistinfoOutput> result = new List<UrcrmportalupdatebundlelistinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdatebundlelistinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdatebundlelistinfoOutput outputobj = new UrcrmportalupdatebundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetorderslistModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetorderslistController : ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetorderslistController> _logger;

        public UrcrmportalgetorderslistController(ILogger<UrcrmportalgetorderslistController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetorderslistOutput>> CreateAsync(UrcrmportalgetorderslistInput req)
        {
            List<UrcrmportalgetorderslistOutput> result = new List<UrcrmportalgetorderslistOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetorderslistDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();                  
            }
            catch (Exception ex)
            {

                UrcrmportalgetorderslistOutput outputobj = new UrcrmportalgetorderslistOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);     
            }
        }

    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdatecompanydevicesinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdatecompanydevicesinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdatecompanydevicesinfoController> _logger;

        public UrcrmportalupdatecompanydevicesinfoController(ILogger<UrcrmportalupdatecompanydevicesinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdatecompanydevicesinfoOutput>> CreateAsync(UrcrmportalupdatecompanydevicesinfoInput req)
        {
            List<UrcrmportalupdatecompanydevicesinfoOutput> result = new List<UrcrmportalupdatecompanydevicesinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdatecompanydevicesinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdatecompanydevicesinfoOutput outputobj = new UrcrmportalupdatecompanydevicesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
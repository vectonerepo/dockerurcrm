﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetticketcommentslogModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetticketcommentslogController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetticketcommentslogController> _logger;

        public UrcrmportalgetticketcommentslogController(ILogger<UrcrmportalgetticketcommentslogController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetticketcommentslogOutput>> CreateAsync(UrcrmportalgetticketcommentslogInput req)
        {
            List<UrcrmportalgetticketcommentslogOutput> result = new List<UrcrmportalgetticketcommentslogOutput>();
            UrcrmportalgetticketcommentslogOutput objSignInInput = new UrcrmportalgetticketcommentslogOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetticketcommentslogDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetticketcommentslogOutput outputobj = new UrcrmportalgetticketcommentslogOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
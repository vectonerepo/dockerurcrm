﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmgetmacaddressModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmgetmacaddressController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmgetmacaddressController> _logger;

        public UrcrmgetmacaddressController(ILogger<UrcrmgetmacaddressController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmgetmacaddressOutput>> CreateAsync(UrcrmgetmacaddressInput req)
        {
            List<UrcrmgetmacaddressOutput> result = new List<UrcrmgetmacaddressOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmgetmacaddressDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmgetmacaddressOutput outputobj = new UrcrmgetmacaddressOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
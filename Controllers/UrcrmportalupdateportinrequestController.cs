﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdateportinrequestModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdateportinrequestController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdateportinrequestController> _logger;

        public UrcrmportalupdateportinrequestController(ILogger<UrcrmportalupdateportinrequestController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdateportinrequestOutput>> CreateAsync(UrcrmportalupdateportinrequestInput req)
        {
            List<UrcrmportalupdateportinrequestOutput> result = new List<UrcrmportalupdateportinrequestOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdateportinrequestDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdateportinrequestOutput outputobj = new UrcrmportalupdateportinrequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
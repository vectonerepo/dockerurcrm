﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmemergcontactupdateModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmemergcontactupdateController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmemergcontactupdateController> _logger;

        public UrcrmemergcontactupdateController(ILogger<UrcrmemergcontactupdateController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmemergcontactupdateOutput>> CreateAsync(UrcrmemergcontactupdateInput req)
        {
            List<UrcrmemergcontactupdateOutput> result = new List<UrcrmemergcontactupdateOutput>();
            try
            {
                result = UrcrmemergcontactupdateDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmemergcontactupdateOutput outputobj = new UrcrmemergcontactupdateOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);

            }
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdatecompanydeviceorderreplacementModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdatecompanydeviceorderreplacementController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdatecompanydeviceorderreplacementController> _logger;

        public UrcrmportalupdatecompanydeviceorderreplacementController(ILogger<UrcrmportalupdatecompanydeviceorderreplacementController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdatecompanydeviceorderreplacementOutput>> CreateAsync(UrcrmportalupdatecompanydeviceorderreplacementInput req)
        {
            List<UrcrmportalupdatecompanydeviceorderreplacementOutput> result = new List<UrcrmportalupdatecompanydeviceorderreplacementOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdatecompanydeviceorderreplacementDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdatecompanydeviceorderreplacementOutput outputobj = new UrcrmportalupdatecompanydeviceorderreplacementOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
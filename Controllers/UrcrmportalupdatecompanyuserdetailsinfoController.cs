﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdatecompanyuserdetailsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdatecompanyuserdetailsinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdatecompanyuserdetailsinfoController> _logger;

        public UrcrmportalupdatecompanyuserdetailsinfoController(ILogger<UrcrmportalupdatecompanyuserdetailsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdatecompanyuserdetailsinfoOutput>> CreateAsync(UrcrmportalupdatecompanyuserdetailsinfoInput req)
        {
            List<UrcrmportalupdatecompanyuserdetailsinfoOutput> result = new List<UrcrmportalupdatecompanyuserdetailsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdatecompanyuserdetailsinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdatecompanyuserdetailsinfoOutput outputobj = new UrcrmportalupdatecompanyuserdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
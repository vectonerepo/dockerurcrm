﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcreatesubpermissionsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcreatesubpermissionsinfoController : ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcreatesubpermissionsinfoController> _logger;

        public UrcrmportalcreatesubpermissionsinfoController(ILogger<UrcrmportalcreatesubpermissionsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcreatesubpermissionsinfoOutput>> CreateAsync(UrcrmportalcreatesubpermissionsinfoInput req)
        {
            List<UrcrmportalcreatesubpermissionsinfoOutput> result = new List<UrcrmportalcreatesubpermissionsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcreatesubpermissionsinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcreatesubpermissionsinfoOutput outputobj = new UrcrmportalcreatesubpermissionsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);
            
            }
        }
    }
}
﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.urcrmportalgetticketsubcategoryModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalgetticketsubcategoryController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetticketsubcategoryController> _logger;

        public urcrmportalgetticketsubcategoryController(ILogger<urcrmportalgetticketsubcategoryController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalgetticketsubcategoryOutput>> CreateAsync(urcrmportalgetticketsubcategoryInput req)
        {
            List<urcrmportalgetticketsubcategoryOutput> result = new List<urcrmportalgetticketsubcategoryOutput>();
           
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetticketsubcategoryDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetticketsubcategoryOutput outputobj = new urcrmportalgetticketsubcategoryOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
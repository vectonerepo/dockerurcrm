﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetpermissionsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetpermissionsinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetpermissionsinfoController> _logger;

        public UrcrmportalgetpermissionsinfoController(ILogger<UrcrmportalgetpermissionsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetpermissionsinfoOutput>> CreateAsync(UrcrmportalgetpermissionsinfoInput req)
        {
            List<UrcrmportalgetpermissionsinfoOutput> result = new List<UrcrmportalgetpermissionsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetpermissionsinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetpermissionsinfoOutput outputobj = new UrcrmportalgetpermissionsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }
        }
    }
}
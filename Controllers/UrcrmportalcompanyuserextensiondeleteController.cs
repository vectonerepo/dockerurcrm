﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcompanyuserextensiondeleteModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcompanyuserextensiondeleteController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcompanyuserextensiondeleteController> _logger;

        public UrcrmportalcompanyuserextensiondeleteController(ILogger<UrcrmportalcompanyuserextensiondeleteController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcompanyuserextensiondeleteOutput>> CreateAsync(UrcrmportalcompanyuserextensiondeleteInput req)
        {
            List<UrcrmportalcompanyuserextensiondeleteOutput> result = new List<UrcrmportalcompanyuserextensiondeleteOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcompanyuserextensiondeleteDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcompanyuserextensiondeleteOutput outputobj = new UrcrmportalcompanyuserextensiondeleteOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj); 
            }
        }
    }
}
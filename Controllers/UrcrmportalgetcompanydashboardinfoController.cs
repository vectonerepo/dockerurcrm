﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalgetcompanydashboardinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetcompanydashboardinfoController : ControllerBase
    {
         public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetcompanydashboardinfoController> _logger;

        public UrcrmportalgetcompanydashboardinfoController(ILogger<UrcrmportalgetcompanydashboardinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetcompanydashboardinfoOutput>> CreateAsync(UrcrmportalgetcompanydashboardinfoInput req)
        {
           UrcrmportalgetcompanydashboardinfoOutput output = new UrcrmportalgetcompanydashboardinfoOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                output = UrcrmportalgetcompanydashboardinfoDB.CallDB(req, _Config, _logger);
                return output.Code == 0 ? Ok(output) : NoContent();
                //return output.Count > 0 ? Ok(output) : NoContent();
            }
            catch (Exception ex)
            {
                UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
                output.Code = -1;
                output.Message = ex.Message;
                return Ok(output);
            }
        }
    }
}
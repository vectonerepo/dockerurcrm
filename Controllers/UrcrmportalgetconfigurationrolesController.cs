﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetconfigurationrolesModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetconfigurationrolesController : ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetconfigurationrolesController> _logger;

        public UrcrmportalgetconfigurationrolesController(ILogger<UrcrmportalgetconfigurationrolesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetconfigurationrolesOutput>> CreateAsync(UrcrmportalgetconfigurationrolesInput req)
        {
            List<UrcrmportalgetconfigurationrolesOutput> result = new List<UrcrmportalgetconfigurationrolesOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetconfigurationrolesDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetconfigurationrolesOutput outputobj = new UrcrmportalgetconfigurationrolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }

        }
    }
}
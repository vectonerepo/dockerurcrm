﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportaluserloginModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportaluserloginController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportaluserloginController> _logger;

        public urcrmportaluserloginController(ILogger<urcrmportaluserloginController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportaluserloginOutput>> CreateAsync(urcrmportaluserloginInput req)
        {
            List<urcrmportaluserloginOutput> result = new List<urcrmportaluserloginOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportaluserloginDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportaluserloginOutput outputobj = new urcrmportaluserloginOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
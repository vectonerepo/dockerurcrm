﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalgetcompanyinvoiceModel;
using URCRMDocker.DB;
namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetcompanyinvoiceController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetcompanyinvoiceController> _logger;

        public UrcrmportalgetcompanyinvoiceController(ILogger<UrcrmportalgetcompanyinvoiceController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetcompanyinvoiceOutput>> CreateAsync(UrcrmportalgetcompanyinvoiceInput req)
        {
            List<UrcrmportalgetcompanyinvoiceOutput> result = new List<UrcrmportalgetcompanyinvoiceOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetcompanyinvoiceDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetcompanyinvoiceOutput outputobj = new UrcrmportalgetcompanyinvoiceOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);        
            }
        }
    }
}
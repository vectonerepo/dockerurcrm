﻿using URCRMDocker.Auth;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using URCRMDocker.DB;
using static URCRMDocker.Models.urcrmportalgetticketPriorityModel;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class urcrmportalgetticketPriorityController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetticketPriorityController> _logger;

        public urcrmportalgetticketPriorityController(ILogger<urcrmportalgetticketPriorityController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<HttpResponseMessage>> Get(HttpRequestMessage req, CancellationToken cancellationToken)
        {
            List<urcrmportalgetticketPriorityOutput> result = new List<urcrmportalgetticketPriorityOutput>();
            urcrmportalgetticketPriorityOutput objSignInInput = new urcrmportalgetticketPriorityOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetticketPriorityDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetticketPriorityOutput outputobj = new urcrmportalgetticketPriorityOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }

        }

    }
}
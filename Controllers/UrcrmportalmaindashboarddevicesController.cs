﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalmaindashboarddevicesModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalmaindashboarddevicesController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalmaindashboarddevicesController> _logger;

        public UrcrmportalmaindashboarddevicesController(ILogger<UrcrmportalmaindashboarddevicesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalmaindashboarddevicesOutput>> CreateAsync(UrcrmportalmaindashboarddevicesInput req)
        {
            List<UrcrmportalmaindashboarddevicesOutput> result = new List<UrcrmportalmaindashboarddevicesOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalmaindashboarddevicesDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalmaindashboarddevicesOutput outputobj = new UrcrmportalmaindashboarddevicesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmupdatemacaddressModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmupdatemacaddressController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmupdatemacaddressController> _logger;

        public UrcrmupdatemacaddressController(ILogger<UrcrmupdatemacaddressController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmupdatemacaddressOutput>> CreateAsync(UrcrmupdatemacaddressInput req)
        {
            List<UrcrmupdatemacaddressOutput> result = new List<UrcrmupdatemacaddressOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmupdatemacaddressDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmupdatemacaddressOutput outputobj = new UrcrmupdatemacaddressOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetticketsModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetticketsController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetticketsController> _logger;

        public UrcrmportalgetticketsController(ILogger<UrcrmportalgetticketsController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetticketsOutput>> CreateAsync(UrcrmportalgetticketsInput req)
        {
            List<UrcrmportalgetticketsOutput> result = new List<UrcrmportalgetticketsOutput>();
            UrcrmportalgetticketsOutput objSignInInput = new UrcrmportalgetticketsOutput();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetticketsDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetticketsOutput outputobj = new UrcrmportalgetticketsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
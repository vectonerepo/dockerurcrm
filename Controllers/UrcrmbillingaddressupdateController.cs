﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmbillingaddressupdateModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmbillingaddressupdateController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<UrcrmbillingaddressupdateController> _logger;

        public UrcrmbillingaddressupdateController(ILogger<UrcrmbillingaddressupdateController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;


        }
        [HttpPost]
        [Authorize]     
        public async Task<ActionResult<UrcrmbillingaddressupdateOutput>> CreateAsync(UrcrmbillingaddressupdateInput req)      
        {
            List<UrcrmbillingaddressupdateOutput> result = new List<UrcrmbillingaddressupdateOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmbillingaddressupdateDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmbillingaddressupdateOutput outputobj = new UrcrmbillingaddressupdateOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                
                return Ok(outputobj);
            }
        }
    }
}
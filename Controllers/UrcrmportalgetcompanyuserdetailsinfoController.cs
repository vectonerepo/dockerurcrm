﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetcompanyuserdetailsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetcompanyuserdetailsinfoController : ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetcompanyuserdetailsinfoController> _logger;

        public UrcrmportalgetcompanyuserdetailsinfoController(ILogger<UrcrmportalgetcompanyuserdetailsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetcompanyuserdetailsinfoOutput>> CreateAsync(UrcrmportalgetcompanyuserdetailsinfoInput req)
        {
            List<UrcrmportalgetcompanyuserdetailsinfoOutput> result = new List<UrcrmportalgetcompanyuserdetailsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetcompanyuserdetailsinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetcompanyuserdetailsinfoOutput outputobj = new UrcrmportalgetcompanyuserdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }
        }
    }
}
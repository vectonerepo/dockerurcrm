﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalgetorderslistfailedModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetorderslistfailedController : ControllerBase
    {


        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetorderslistfailedController> _logger;

        public UrcrmportalgetorderslistfailedController(ILogger<UrcrmportalgetorderslistfailedController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UrcrmportalgetorderslistfailedOutput>> CreateAsync()
        {
            List<UrcrmportalgetorderslistfailedOutput> result = new List<UrcrmportalgetorderslistfailedOutput>();
           
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetorderslistfailedDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();           
            }
            catch (Exception ex)
            {
                UrcrmportalgetorderslistfailedOutput outputobj = new UrcrmportalgetorderslistfailedOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);         
            }
        }
    }
}
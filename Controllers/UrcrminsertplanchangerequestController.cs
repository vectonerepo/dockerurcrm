﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrminsertplanchangerequestModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrminsertplanchangerequestController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrminsertplanchangerequestController> _logger;

        public UrcrminsertplanchangerequestController(ILogger<UrcrminsertplanchangerequestController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrminsertplanchangerequestOutput>> CreateAsync(UrcrminsertplanchangerequestInput req)
        {
            List<UrcrminsertplanchangerequestOutput> result = new List<UrcrminsertplanchangerequestOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrminsertplanchangerequestDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrminsertplanchangerequestOutput outputobj = new UrcrminsertplanchangerequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportalloginforgotpwdModel;
using URCRMDocker.DB;
using System.IO;
using System.Net.Mail;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalloginforgotpwdController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalloginforgotpwdController> _logger;

        public urcrmportalloginforgotpwdController(ILogger<urcrmportalloginforgotpwdController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalloginforgotpwdOutput>> CreateAsync(urcrmportalloginforgotpwdInput req)
        {
            List<urcrmportalloginforgotpwdOutput> result = new List<urcrmportalloginforgotpwdOutput>();
            try
            { //Call SQL DB to verify credentials validation 
                result = urcrmportalloginforgotpwdDB.CallDB(req, _Config, _logger);
                if (result[0].Code == 0 && req.type == 2)
                {
                    //Send Mail
                    _logger.LogInformation("Send Mail..");
                    string URL = _Config["MailConfigSection:EmailResetURL"];
                    string usermailId_Encrypted = Base64Encode(req.email);
                    string medium = Base64Encode("WEB");
                    string frame = Base64Encode("");
                    SendMail(URL, usermailId_Encrypted, medium, "", "Reset your UnifiedRing password", req.email, result[0].first_name, frame);
                }
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalloginforgotpwdOutput outputobj = new urcrmportalloginforgotpwdOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                return Ok(outputobj);
            }
        }
        [HttpGet]
        public void SendMail(string requestUrl, string payload, string medium, string msisdn, string subject, string toaddress, string firstname, string frame)
        {
            try
            {
                string from = _Config["MailConfigSection:FromList"];
                StreamReader mailContent_file = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "UnifiedRing.html"));
                string body = requestUrl + payload + "&medium=" + medium + "&frame=" + frame;
                string mailContent = mailContent_file.ReadToEnd();
                mailContent = mailContent.Replace("*|reset_url|*", body);
                mailContent = mailContent.Replace("*|SUBJECT|*", subject);
                mailContent = mailContent.Replace("*|UserName|*", firstname);
                mailContent = mailContent.Replace("*|MC_PREVIEW_TEXT|*", "Reset Password");
                mailContent_file.Close();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(toaddress);
                mail.From = new MailAddress(from, "Unified ring");
                mail.Subject = subject + msisdn;
                mail.IsBodyHtml = true;
                mail.Body = mailContent;
                SmtpClient emailClient = new SmtpClient(_Config["MailConfigSection:SmtpHost"]);
                emailClient.Credentials = new System.Net.NetworkCredential(_Config["MailConfigSection:SmtpUser"], _Config["MailConfigSection:SmtpPass"]);
                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Send Mail failed..", ex.Message.ToString());

            }
            _logger.LogInformation("Send Mail completed..");
        }
        [HttpGet]
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        
    }
}
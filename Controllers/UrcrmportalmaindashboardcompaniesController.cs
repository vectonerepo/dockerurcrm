﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalmaindashboardcompaniesModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalmaindashboardcompaniesController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalmaindashboardcompaniesController> _logger;

        public UrcrmportalmaindashboardcompaniesController(ILogger<UrcrmportalmaindashboardcompaniesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalmaindashboardcompaniesOutput>> CreateAsync(UrcrmportalmaindashboardcompaniesInput req)
        {
            List<UrcrmportalmaindashboardcompaniesOutput> result = new List<UrcrmportalmaindashboardcompaniesOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalmaindashboardcompaniesDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalmaindashboardcompaniesOutput outputobj = new UrcrmportalmaindashboardcompaniesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetportinstatusdrpModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetportinstatusdrpController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetportinstatusdrpController> _logger;

        public UrcrmportalgetportinstatusdrpController(ILogger<UrcrmportalgetportinstatusdrpController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UrcrmportalgetportinstatusdrpOutput>> CreateAsync()
        {
            List<UrcrmportalgetportinstatusdrpOutput> result = new List<UrcrmportalgetportinstatusdrpOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetportinstatusdrpDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetportinstatusdrpOutput outputobj = new UrcrmportalgetportinstatusdrpOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);         
            }

        }
    }
}
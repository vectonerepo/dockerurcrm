﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalmaindashboardpaymentdueModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalmaindashboardpaymentdueController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalmaindashboardpaymentdueController> _logger;

        public UrcrmportalmaindashboardpaymentdueController(ILogger<UrcrmportalmaindashboardpaymentdueController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalmaindashboardpaymentdueOutput>> CreateAsync(UrcrmportalmaindashboardpaymentdueInput req)
        {
            List<UrcrmportalmaindashboardpaymentdueOutput> result = new List<UrcrmportalmaindashboardpaymentdueOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalmaindashboardpaymentdueDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalmaindashboardpaymentdueOutput outputobj = new UrcrmportalmaindashboardpaymentdueOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
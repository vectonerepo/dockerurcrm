﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetportinrequestModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetportinrequestController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetportinrequestController> _logger;

        public UrcrmportalgetportinrequestController(ILogger<UrcrmportalgetportinrequestController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetportinrequestOutput>> CreateAsync(UrcrmportalgetportinrequestInput req)
        {
            List<UrcrmportalgetportinrequestOutput> result = new List<UrcrmportalgetportinrequestOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetportinrequestDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetportinrequestOutput outputobj = new UrcrmportalgetportinrequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }
        }
    }
}
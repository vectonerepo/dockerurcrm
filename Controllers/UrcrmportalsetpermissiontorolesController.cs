﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalsetpermissiontorolesModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalsetpermissiontorolesController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalsetpermissiontorolesController> _logger;

        public UrcrmportalsetpermissiontorolesController(ILogger<UrcrmportalsetpermissiontorolesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalsetpermissiontorolesOutput>> CreateAsync(UrcrmportalsetpermissiontorolesInput req)
        {
            List<UrcrmportalsetpermissiontorolesOutput> result = new List<UrcrmportalsetpermissiontorolesOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalsetpermissiontorolesDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalsetpermissiontorolesOutput outputobj = new UrcrmportalsetpermissiontorolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
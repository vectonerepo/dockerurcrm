﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalmaindashboardplanstatusModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalmaindashboardplanstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalmaindashboardplanstatusController> _logger;

        public UrcrmportalmaindashboardplanstatusController(ILogger<UrcrmportalmaindashboardplanstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalmaindashboardplanstatusOutput>> CreateAsync(UrcrmportalmaindashboardplanstatusInput req)
        {
           UrcrmportalmaindashboardplanstatusOutput result = new UrcrmportalmaindashboardplanstatusOutput(); 
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalmaindashboardplanstatusDB.CallDB(req, _Config, _logger);
                //return result.PlanDetails.Count > 0 || result.DateDetails.Count> 0 || result.Code != -1  ? Ok(result) : NoContent();
                return result.Code == 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalmaindashboardplanstatusOutput outputobj = new UrcrmportalmaindashboardplanstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message; 
                return Ok(outputobj);
            }

        }
    }
}
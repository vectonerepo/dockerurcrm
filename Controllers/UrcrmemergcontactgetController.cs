﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmemergcontactgetModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmemergcontactgetController: ControllerBase
    {
        public IConfiguration _Config { get; }
        private readonly ILogger<UrcrmemergcontactgetController> _logger;

        public UrcrmemergcontactgetController(ILogger<UrcrmemergcontactgetController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;


        }
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UrcrmemergcontactgetOutput>> CreateAsync(UrcrmemergcontactgetInput req)
        {
            List<UrcrmemergcontactgetOutput> result = new List<UrcrmemergcontactgetOutput>();
            try
            {
                result = UrcrmemergcontactgetDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmemergcontactgetOutput outputobj = new UrcrmemergcontactgetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmdeletecompanyinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmdeletecompanyinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmdeletecompanyinfoController> _logger;

        public UrcrmdeletecompanyinfoController(ILogger<UrcrmdeletecompanyinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]

        public async Task<ActionResult<UrcrmdeletecompanyinfoOutput>> CreateAsync(UrcrmdeletecompanyinfoInput req)
        {
            List<UrcrmdeletecompanyinfoOutput> result = new List<UrcrmdeletecompanyinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmdeletecompanyinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmdeletecompanyinfoOutput outputobj = new UrcrmdeletecompanyinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
                _logger.LogError("Output Error: " + ex.Message);
                return Ok(outputobj);
            }
        }
    }
}
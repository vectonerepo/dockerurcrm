﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalvalidatebundlesubscribeModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalvalidatebundlesubscribeController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalvalidatebundlesubscribeController> _logger;

        public UrcrmportalvalidatebundlesubscribeController(ILogger<UrcrmportalvalidatebundlesubscribeController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalvalidatebundlesubscribeOutput>> CreateAsync(UrcrmportalvalidatebundlesubscribeInput req)
        {
            List<UrcrmportalvalidatebundlesubscribeOutput> result = new List<UrcrmportalvalidatebundlesubscribeOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalvalidatebundlesubscribeDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalvalidatebundlesubscribeOutput outputobj = new UrcrmportalvalidatebundlesubscribeOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
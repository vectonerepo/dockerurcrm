﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalgetnewplanlistModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetnewplanlistController: ControllerBase
    {


        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetnewplanlistController> _logger;

        public UrcrmportalgetnewplanlistController(ILogger<UrcrmportalgetnewplanlistController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UrcrmportalgetnewplanlistOutput>> CreateAsync()
        {
            List<UrcrmportalgetnewplanlistOutput> result = new List<UrcrmportalgetnewplanlistOutput>();
           
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetnewplanlistDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();           
            }
            catch (Exception ex)
            {
                UrcrmportalgetnewplanlistOutput outputobj = new UrcrmportalgetnewplanlistOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalgetsubpermissionsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalgetsubpermissionsinfoController : ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalgetsubpermissionsinfoController> _logger;

        public UrcrmportalgetsubpermissionsinfoController(ILogger<UrcrmportalgetsubpermissionsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalgetsubpermissionsinfoOutput>> CreateAsync(UrcrmportalgetsubpermissionsinfoInput req)
        {
            List<UrcrmportalgetsubpermissionsinfoOutput> result = new List<UrcrmportalgetsubpermissionsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalgetsubpermissionsinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalgetsubpermissionsinfoOutput outputobj = new UrcrmportalgetsubpermissionsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
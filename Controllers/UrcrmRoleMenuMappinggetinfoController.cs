﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmRoleMenuMappinggetinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmRoleMenuMappinggetinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmRoleMenuMappinggetinfoController> _logger;

        public UrcrmRoleMenuMappinggetinfoController(ILogger<UrcrmRoleMenuMappinggetinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmRoleMenuMappinggetinfoOutput>> CreateAsync(UrcrmRoleMenuMappinggetinfoInput req)
        {
            List<UrcrmRoleMenuMappinggetinfoOutput> result = new List<UrcrmRoleMenuMappinggetinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmRoleMenuMappinggetinfoDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmRoleMenuMappinggetinfoOutput outputobj = new UrcrmRoleMenuMappinggetinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
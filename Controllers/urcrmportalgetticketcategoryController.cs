﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportalgetticketcategoryModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalgetticketcategoryController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetticketcategoryController> _logger;

        public urcrmportalgetticketcategoryController(ILogger<urcrmportalgetticketcategoryController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalgetticketcategoryOutput>> CreateAsync(urcrmportalgetticketcategoryInput req)
        {
            List<urcrmportalgetticketcategoryOutput> result = new List<urcrmportalgetticketcategoryOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetticketcategoryDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetticketcategoryOutput outputobj = new urcrmportalgetticketcategoryOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
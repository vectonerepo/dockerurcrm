﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.UrcrmportalupdatecompanydeviceorderreturnModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalupdatecompanydeviceorderreturnController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalupdatecompanydeviceorderreturnController> _logger;

        public UrcrmportalupdatecompanydeviceorderreturnController(ILogger<UrcrmportalupdatecompanydeviceorderreturnController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalupdatecompanydeviceorderreturnOutput>> CreateAsync(UrcrmportalupdatecompanydeviceorderreturnInput req)
        {
            List<UrcrmportalupdatecompanydeviceorderreturnOutput> result = new List<UrcrmportalupdatecompanydeviceorderreturnOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalupdatecompanydeviceorderreturnDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalupdatecompanydeviceorderreturnOutput outputobj = new UrcrmportalupdatecompanydeviceorderreturnOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
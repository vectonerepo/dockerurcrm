﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcompanydeviceorderreplacementgetModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcompanydeviceorderreplacementgetController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcompanydeviceorderreplacementgetController> _logger;

        public UrcrmportalcompanydeviceorderreplacementgetController(ILogger<UrcrmportalcompanydeviceorderreplacementgetController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcompanydeviceorderreplacementgetOutput>> CreateAsync(UrcrmportalcompanydeviceorderreplacementgetInput req)
        {
            List<UrcrmportalcompanydeviceorderreplacementgetOutput> result = new List<UrcrmportalcompanydeviceorderreplacementgetOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcompanydeviceorderreplacementgetDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcompanydeviceorderreplacementgetOutput outputobj = new UrcrmportalcompanydeviceorderreplacementgetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);         
            }
        }
    }
}
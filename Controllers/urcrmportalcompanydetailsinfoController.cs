﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.urcrmportalcompanydetailsinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalcompanydetailsinfoController : ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalcompanydetailsinfoController> _logger;

        public urcrmportalcompanydetailsinfoController(ILogger<urcrmportalcompanydetailsinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalcompanydetailsinfoOutput>> CreateAsync(urcrmportalcompanydetailsinfoInput req)
        {
            List<urcrmportalcompanydetailsinfoOutput> result = new List<urcrmportalcompanydetailsinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalcompanydetailsinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalcompanydetailsinfoOutput outputobj = new urcrmportalcompanydetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);            
            }
        }
    }
}
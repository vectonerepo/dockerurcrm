﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportalgetuserbydeptModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalgetuserbydeptController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetuserbydeptController> _logger;

        public urcrmportalgetuserbydeptController(ILogger<urcrmportalgetuserbydeptController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<urcrmportalgetuserbydeptOutput>> CreateAsync(urcrmportalgetuserbydeptInput req)
        {
            List<urcrmportalgetuserbydeptOutput> result = new List<urcrmportalgetuserbydeptOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetuserbydeptDB.CallDB(req,_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetuserbydeptOutput outputobj = new urcrmportalgetuserbydeptOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
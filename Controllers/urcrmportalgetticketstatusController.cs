﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using static URCRMDocker.Models.urcrmportalgetticketstatusModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class urcrmportalgetticketstatusController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<urcrmportalgetticketstatusController> _logger;

        public urcrmportalgetticketstatusController(ILogger<urcrmportalgetticketstatusController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<urcrmportalgetticketstatusOutput>> CreateAsync()
        {
            List<urcrmportalgetticketstatusOutput> result = new List<urcrmportalgetticketstatusOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = urcrmportalgetticketstatusDB.CallDB(_Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                urcrmportalgetticketstatusOutput outputobj = new urcrmportalgetticketstatusOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
               return Ok(outputobj);         
            }

        }
    }
}
﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcreatecompanyemailinfoModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcreatecompanyemailinfoController: ControllerBase
    {
        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcreatecompanyemailinfoController> _logger;

        public UrcrmportalcreatecompanyemailinfoController(ILogger<UrcrmportalcreatecompanyemailinfoController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcreatecompanyemailinfoOutput>> CreateAsync(UrcrmportalcreatecompanyemailinfoInput req)
        {
            List<UrcrmportalcreatecompanyemailinfoOutput> result = new List<UrcrmportalcreatecompanyemailinfoOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcreatecompanyemailinfoDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcreatecompanyemailinfoOutput outputobj = new UrcrmportalcreatecompanyemailinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);        
            }
        }
    }
}
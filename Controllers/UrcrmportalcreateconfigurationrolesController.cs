﻿using URCRMDocker.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using static URCRMDocker.Models.UrcrmportalcreateconfigurationrolesModel;
using URCRMDocker.DB;

namespace URCRMDocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrcrmportalcreateconfigurationrolesController: ControllerBase
    {

        public IConfiguration _Config { get; }

        private readonly ILogger<UrcrmportalcreateconfigurationrolesController> _logger;

        public UrcrmportalcreateconfigurationrolesController(ILogger<UrcrmportalcreateconfigurationrolesController> logger, IConfiguration configuration)
        {
            _Config = configuration; _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<UrcrmportalcreateconfigurationrolesOutput>> CreateAsync(UrcrmportalcreateconfigurationrolesInput req)
        {
            List<UrcrmportalcreateconfigurationrolesOutput> result = new List<UrcrmportalcreateconfigurationrolesOutput>();
            try
            {
                //Call SQL DB to verify credentials validation 
                result = UrcrmportalcreateconfigurationrolesDB.CallDB(req, _Config, _logger);
                return result.Count > 0 ? Ok(result) : NoContent();
            }
            catch (Exception ex)
            {

                UrcrmportalcreateconfigurationrolesOutput outputobj = new UrcrmportalcreateconfigurationrolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                result.Add(outputobj);
                _logger.LogError("Output Error: " + ex.Message);
               return Ok(outputobj);
            
            }
        }
    }
}
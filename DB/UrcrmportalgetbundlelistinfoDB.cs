﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetbundlelistinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetbundlelistinfoDB
    {
        
        public static List<UrcrmportalgetbundlelistinfoOutput> CallDB(UrcrmportalgetbundlelistinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetbundlelistinfoOutput> OutputList = new List<UrcrmportalgetbundlelistinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetbundlelistinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_bundle_list_info";
                    object param = new
                            
                            {
                                @bundleid = req.bundleid

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetbundlelistinfoOutput()
                        {
                            id = r.id == null ? 0 : r.id,
                            bundleid = r.bundleid == null ? 0 : r.bundleid,
                            bundle_name = r.bundle_name,
                            price = r.price == null ? 0.0D : r.price,
                            national_min = r.national_min == null ? 0.0D : r.national_min,
                            international_min = r.international_min == null ? 0.0D : r.international_min,
                            bundle_status = r.bundle_status == null ? 0 : r.bundle_status,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetbundlelistinfoOutput outputobj = new UrcrmportalgetbundlelistinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetbundlelistinfoOutput outputobj = new UrcrmportalgetbundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
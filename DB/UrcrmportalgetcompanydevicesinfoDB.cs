﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanydevicesinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanydevicesinfoDB
    {
        
        public static List<UrcrmportalgetcompanydevicesinfoOutput> CallDB(UrcrmportalgetcompanydevicesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanydevicesinfoOutput> OutputList = new List<UrcrmportalgetcompanydevicesinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanydevicesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_devices_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanydevicesinfoOutput()
                        {
                            order_id = r.order_id == null ? 0 : r.order_id,
                            model_name = r.model_name,
                            model_number = r.model_number,
                            price = r.price == null ? 0.0D : r.price,
                            order_placed_by = r.order_placed_by,
                            order_status = r.order_status == null ? 0 : r.order_status,
                            mac_id = r.mac_id,
                            Ordered_On = r.Ordered_On == null ? null : r.Ordered_On,
                            last_update = r.last_update == null ? null : r.last_update,
                            category = r.category,
                            referenceid = r.referenceid,
                            tracking_no = r.tracking_no,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            delivery_comment = r.delivery_comment,
                            comment = r.comment,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));

                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanydevicesinfoOutput outputobj = new UrcrmportalgetcompanydevicesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanydevicesinfoOutput outputobj = new UrcrmportalgetcompanydevicesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
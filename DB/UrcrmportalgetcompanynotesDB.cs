﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanynotesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanynotesDB
    {
        
        public static List<UrcrmportalgetcompanynotesOutput> CallDB(UrcrmportalgetcompanynotesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanynotesOutput> OutputList = new List<UrcrmportalgetcompanynotesOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanynotesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_notes";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanynotesOutput()
                        {
                            Notes_id = r.Notes_id == null ? 0 : r.Notes_id,
                            Company_id = r.Company_id == null ? 0 : r.Company_id,
                            Notes_subject = r.Notes_subject,
                            Notes_description = r.Notes_description,
                            created_date = r.created_date == null ? null : r.created_date,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanynotesOutput outputobj = new UrcrmportalgetcompanynotesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanynotesOutput outputobj = new UrcrmportalgetcompanynotesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
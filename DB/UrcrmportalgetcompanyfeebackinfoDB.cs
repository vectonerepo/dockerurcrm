﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanyfeebackinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanyfeebackinfoDB
    {

        
        public static List<UrcrmportalgetcompanyfeebackinfoOutput> CallDB(UrcrmportalgetcompanyfeebackinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanyfeebackinfoOutput> OutputList = new List<UrcrmportalgetcompanyfeebackinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanyfeebackinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_feeback_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanyfeebackinfoOutput()
                        {
                            company_id = r.company_id == null ? 0 : r.company_id,
                            company_rating = r.company_rating == null ? 0 : r.company_rating,
                            feedback_title = r.feedback_title,
                            feedback_review = r.feedback_review,
                            createddate = r.createddate == null ? null : r.createddate,
                            called_by = r.called_by,
                            submitted_by = r.submitted_by,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanyfeebackinfoOutput outputobj = new UrcrmportalgetcompanyfeebackinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanyfeebackinfoOutput outputobj = new UrcrmportalgetcompanyfeebackinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
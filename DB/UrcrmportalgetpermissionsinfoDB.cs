﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetpermissionsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetpermissionsinfoDB
    {
        
        public static List<UrcrmportalgetpermissionsinfoOutput> CallDB(UrcrmportalgetpermissionsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetpermissionsinfoOutput> OutputList = new List<UrcrmportalgetpermissionsinfoOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_permissions_info";
                object param = new
                        
                        {
                            @permission_id = req.permission_id
                        };
                       
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UrcrmportalgetpermissionsinfoOutput()
                    {

                        permission_id = r.permission_id == null ? 0 : r.permission_id,
                        permission_name = r.permission_name,
                        permission_status = r.permission_status == null ? 0 : r.permission_status,
                        permission_link = r.permission_link,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UrcrmportalgetpermissionsinfoOutput outputobj = new UrcrmportalgetpermissionsinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
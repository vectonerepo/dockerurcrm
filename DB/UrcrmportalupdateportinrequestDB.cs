﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdateportinrequestModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalupdateportinrequestDB
    {
        
        public static List<UrcrmportalupdateportinrequestOutput> CallDB(UrcrmportalupdateportinrequestInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdateportinrequestOutput> OutputList = new List<UrcrmportalupdateportinrequestOutput>();
           _logger.LogInformation("Input : UrcrmportalupdateportinrequestController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_portin_request";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @reqid = req.reqid,
                                @portin_status = req.portin_status,
                                @comments = req.comments
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdateportinrequestOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdateportinrequestOutput outputobj = new UrcrmportalupdateportinrequestOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdateportinrequestOutput outputobj = new UrcrmportalupdateportinrequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
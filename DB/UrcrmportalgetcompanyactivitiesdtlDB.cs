﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanyactivitiesdtlModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanyactivitiesdtlDB
    {
        
        public static List<UrcrmportalgetcompanyactivitiesdtlOutput> CallDB(UrcrmportalgetcompanyactivitiesdtlInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanyactivitiesdtlOutput> OutputList = new List<UrcrmportalgetcompanyactivitiesdtlOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanyactivitiesdtlController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_activities_dtl";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @userid = req.userid
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanyactivitiesdtlOutput()
                        {
                            createdate = r.createdate == null ? null : r.createdate,
                            name = r.name,
                            activity = r.activity,
                            type = r.type,
                            status = r.status,
                            username = r.username,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanyactivitiesdtlOutput outputobj = new UrcrmportalgetcompanyactivitiesdtlOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanyactivitiesdtlOutput outputobj = new UrcrmportalgetcompanyactivitiesdtlOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
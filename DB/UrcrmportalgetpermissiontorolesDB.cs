﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetpermissiontorolesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetpermissiontorolesDB
    {
        
        public static List<UrcrmportalgetpermissiontorolesOutput> CallDB(UrcrmportalgetpermissiontorolesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetpermissiontorolesOutput> OutputList = new List<UrcrmportalgetpermissiontorolesOutput>();
           _logger.LogInformation("Input : UrcrmportalgetpermissiontorolesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_permission_to_roles";
                    object param = new
                            
                            {
                                @Role_id = req.Role_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetpermissiontorolesOutput()
                        {
                            Role_name = r.Role_name,
                            description = r.description,
                            sub_perm_id = r.sub_perm_id == null ? 0 : r.sub_perm_id,
                            sub_perm_name = r.sub_perm_name,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetpermissiontorolesOutput outputobj = new UrcrmportalgetpermissiontorolesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetpermissiontorolesOutput outputobj = new UrcrmportalgetpermissiontorolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
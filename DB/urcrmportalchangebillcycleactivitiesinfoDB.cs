﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalchangebillcycleactivitiesinfoModel;

namespace URCRMDocker.DB
{
    public class urcrmportalchangebillcycleactivitiesinfoDB
    {
        
        public static List<urcrmportalchangebillcycleactivitiesinfoOutput> CallDB(urcrmportalchangebillcycleactivitiesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalchangebillcycleactivitiesinfoOutput> OutputList = new List<urcrmportalchangebillcycleactivitiesinfoOutput>();
           _logger.LogInformation("Input : urcrmportalchangebillcycleactivitiesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_change_bill_cycle_activities_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,                               
                                @bill_cycle = req.bill_cycle,
                                @id = req.id,
                                @processtype = req.processtype,
                                @plan_subscribe = req.plan_subscribe,
                                @reference_no = req.reference_no,
                                @bundle_cycle_description = req.bundle_cycle_description,
                                @bundle_cycle_charge = req.bundle_cycle_charge,
                                @adjustment_charge = req.adjustment_charge,
                                @tax_fee = req.tax_fee,
                                @tax_per = req.tax_per,
                                @total_bundle_cycle_charge = req.total_bundle_cycle_charge,
                                @userid = req.userid                               
                            };
                            

                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalchangebillcycleactivitiesinfoOutput()
                        {
                           // next_bill_date = r.next_bill_date == null ? null : r.next_bill_date,
                            bill_cycle = r.bill_cycle,
                            cp_id = r.cp_id,
                            next_bill_date = r.next_bill_date,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalchangebillcycleactivitiesinfoOutput outputobj = new urcrmportalchangebillcycleactivitiesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalchangebillcycleactivitiesinfoOutput outputobj = new urcrmportalchangebillcycleactivitiesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalsmesubscribebundleespModel;
namespace URCRMDocker.DB
{
    public class UrcrmportalsmesubscribebundleespDB
    {
        
        public static List<UrcrmportalsmesubscribebundleespOutput> CallDB(UrcrmportalsmesubscribebundleespInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalsmesubscribebundleespOutput> OutputList = new List<UrcrmportalsmesubscribebundleespOutput>();
           _logger.LogInformation("Input : UrcrmportalsmesubscribebundleespController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_sme_subscribe_bundle_esp";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @sitecode = req.sitecode,
                                @enterpriseid = req.enterpriseid,
                                @bundleid = req.bundleid,
                                @paymode = req.paymode,
                                @processby = req.processby,
                                @bundle_description = req.bundle_description,
                                @bundle_charge = req.bundle_charge,
                                @userid = req.userid,
                                @adjustment_charge = req.adjustment_charge,
                                @tax_fee = req.tax_fee,
                                @tax_per = req.tax_per,
                                @total_bundle_charge = req.total_bundle_charge,
                                @reference_no = req.reference_no
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalsmesubscribebundleespOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalsmesubscribebundleespOutput outputobj = new UrcrmportalsmesubscribebundleespOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalsmesubscribebundleespOutput outputobj = new UrcrmportalsmesubscribebundleespOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetuserroleinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetuserroleinfoDB
    {
        
        public static List<UrcrmportalgetuserroleinfoOutput> CallDB(UrcrmportalgetuserroleinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetuserroleinfoOutput> OutputList = new List<UrcrmportalgetuserroleinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetuserroleinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_userrole_info";
                    object param = new
                            
                            {
                                @user_id = req.user_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetuserroleinfoOutput()
                        {
                            userid = r.userid == null ? 0 : r.userid,
                            username = r.username,
                            Role_name = r.Role_name,
                            user_role = r.user_role == null ? 0 : r.user_role,
                            status = r.status == null ? 0 : r.status,
                            createdate = r.createdate == null ? null : r.createdate,
                            password =r.password,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetuserroleinfoOutput outputobj = new UrcrmportalgetuserroleinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetuserroleinfoOutput outputobj = new UrcrmportalgetuserroleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreateconfigurationrolesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreateconfigurationrolesDB
    {
        
        public static List<UrcrmportalcreateconfigurationrolesOutput> CallDB(UrcrmportalcreateconfigurationrolesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreateconfigurationrolesOutput> OutputList = new List<UrcrmportalcreateconfigurationrolesOutput>();
           _logger.LogInformation("Input : UrcrmportalcreateconfigurationrolesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_configuration_roles";
                    object param = new
                            
                            {
                                @Processtype = req.Processtype,
                                @Role_name = req.Role_name,
                                @Description = req.Description,
                                @Role_status = req.Role_status,
                                @Role_id =  req.Role_id,
                                @features = req.features
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreateconfigurationrolesOutput()
                        {                           
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreateconfigurationrolesOutput outputobj = new UrcrmportalcreateconfigurationrolesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreateconfigurationrolesOutput outputobj = new UrcrmportalcreateconfigurationrolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcompanydeviceorderreturngetModel;
namespace URCRMDocker.DB
{
    public class UrcrmportalcompanydeviceorderreturngetDB
    {
        
        public static List<UrcrmportalcompanydeviceorderreturngetOutput> CallDB(UrcrmportalcompanydeviceorderreturngetInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcompanydeviceorderreturngetOutput> OutputList = new List<UrcrmportalcompanydeviceorderreturngetOutput>();
           _logger.LogInformation("Input : UrcrmportalcompanydeviceorderreturngetController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_company_device_order_return_get";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcompanydeviceorderreturngetOutput()
                        {
                            id = r.id == null ? 0 : r.id,
                            order_id = r.order_id == null ? 0 : r.order_id,
                            prod_code = r.prod_code,
                            prod_name = r.prod_name,
                            prod_model = r.prod_model,
                            rtn_date = r.rtn_date == null ? null : r.rtn_date,
                            rtn_quantity = r.rtn_quantity == null ? 0 : r.rtn_quantity,
                            rtn_charge = r.rtn_charge == null ? 0.0D : r.rtn_charge,
                            rtn_reason = r.rtn_reason,
                            rtn_status = r.rtn_status == null ? 0 : r.rtn_status,
                            tracking_no = r.tracking_no,
                            comments = r.comments,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            company_name = r.company_name,
                            status_desc = r.status_desc,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcompanydeviceorderreturngetOutput outputobj = new UrcrmportalcompanydeviceorderreturngetOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcompanydeviceorderreturngetOutput outputobj = new UrcrmportalcompanydeviceorderreturngetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
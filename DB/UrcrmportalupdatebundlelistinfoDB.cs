﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdatebundlelistinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalupdatebundlelistinfoDB
    {
        
        public static List<UrcrmportalupdatebundlelistinfoOutput> CallDB(UrcrmportalupdatebundlelistinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdatebundlelistinfoOutput> OutputList = new List<UrcrmportalupdatebundlelistinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalupdatebundlelistinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_bundle_list_info";
                    object param = new
                            
                            {
                                @bundleid = req.bundleid,
                                @bundle_status = req.bundle_status,
                                @bundle_name = req.bundle_name,
                                @price = req.price,
                                @national_min = req.national_min,
                                @international_min = req.international_min,
                                @Reseller_flag = req.Reseller_flag,
                                @bundle_type = req.bundle_type,
                    };


                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdatebundlelistinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdatebundlelistinfoOutput outputobj = new UrcrmportalupdatebundlelistinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdatebundlelistinfoOutput outputobj = new UrcrmportalupdatebundlelistinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
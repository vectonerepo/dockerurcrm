﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreateuserroleinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreateuserroleinfoDB
    {
        
        public static List<UrcrmportalcreateuserroleinfoOutput> CallDB(UrcrmportalcreateuserroleinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreateuserroleinfoOutput> OutputList = new List<UrcrmportalcreateuserroleinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcreateuserroleinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_userrole_info";
                    object param = new
                            
                            {
                                @username_email = req.username_email,
                                @password = req.password,
                                @user_role_id = req.user_role_id,
                                @user_status = req.user_status,
                                @user_id = req.user_id,
                                @dept_id = req.dept_id,
                                @processtype = req.processtype
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreateuserroleinfoOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreateuserroleinfoOutput outputobj = new UrcrmportalcreateuserroleinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreateuserroleinfoOutput outputobj = new UrcrmportalcreateuserroleinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
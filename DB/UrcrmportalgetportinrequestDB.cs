﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetportinrequestModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetportinrequestDB
    {
        
        public static List<UrcrmportalgetportinrequestOutput> CallDB(UrcrmportalgetportinrequestInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetportinrequestOutput> OutputList = new List<UrcrmportalgetportinrequestOutput>();
           _logger.LogInformation("Input : UrcrmportalgetportinrequestController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_portin_request";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @reqid = req.reqid
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetportinrequestOutput()
                        {

                            order_number = r.order_number,
                            req_from = r.req_from,
                            create_date = r.create_date == null ? null : r.create_date,
                            req_to = r.req_to,
                            number_type = r.number_type,
                            service_provider = r.service_provider,
                            status = r.status,
                            status_id = r.status_id == null ? 0 : r.status_id,
                            cupid_id = r.cupid_id,
                            upload_path = r.upload_path,
                            signedLOA_path = r.signedLOA_path,
                            transfer_date = r.transfer_date == null ? null : r.transfer_date,
                            transfer_no = r.transfer_no,
                            assigned_to  = r.assigned_to,
                            replace_no = r.replace_no,
                            reqid = r.reqid == null ? 0 : r.reqid,
                            comments = r.comments,                          
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetportinrequestOutput outputobj = new UrcrmportalgetportinrequestOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetportinrequestOutput outputobj = new UrcrmportalgetportinrequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
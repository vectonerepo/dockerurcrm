﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetuserbydeptModel;
namespace URCRMDocker.DB
{
    public class urcrmportalgetuserbydeptDB
    {
        
        public static List<urcrmportalgetuserbydeptOutput> CallDB(urcrmportalgetuserbydeptInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetuserbydeptOutput> OutputList = new List<urcrmportalgetuserbydeptOutput>();
           _logger.LogInformation("Input : urcrmportalgetuserbydeptController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_user_by_dept";
                    object param = new
                            
                            {
                                @dept_id = req.dept_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetuserbydeptOutput()
                        {
                            userid = r.userid == null ? 0 : r.userid,
                            username = r.username,
                            email = r.email,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetuserbydeptOutput outputobj = new urcrmportalgetuserbydeptOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetuserbydeptOutput outputobj = new urcrmportalgetuserbydeptOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreatepermissionsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreatepermissionsinfoDB
    {
        
        public static List<UrcrmportalcreatepermissionsinfoOutput> CallDB(UrcrmportalcreatepermissionsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreatepermissionsinfoOutput> OutputList = new List<UrcrmportalcreatepermissionsinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcreatepermissionsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_permissions_info";
                    object param = new
                            
                            {
                                @permission_id = req.permission_id,
                                @permission_name = req.permission_name,
                                @permission_status = req.permission_status,
                                @processtype = req.processtype,
                                @permission_link = req.@permission_link

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreatepermissionsinfoOutput()
                        {                          
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreatepermissionsinfoOutput outputobj = new UrcrmportalcreatepermissionsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreatepermissionsinfoOutput outputobj = new UrcrmportalcreatepermissionsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
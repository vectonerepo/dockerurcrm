﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalbillgetdirectdebitaccountModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalbillgetdirectdebitaccountDB
    {
        
        public static List<UrcrmportalbillgetdirectdebitaccountOutput> CallDB(UrcrmportalbillgetdirectdebitaccountInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalbillgetdirectdebitaccountOutput> OutputList = new List<UrcrmportalbillgetdirectdebitaccountOutput>();
           _logger.LogInformation("Input : UrcrmportalbillgetdirectdebitaccountController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_bill_get_directdebit_account";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalbillgetdirectdebitaccountOutput()
                        {
                            firstname = r.firstname,
                            surname = r.surname,
                            mobileno = r.mobileno,
                            email = r.email,
                            address1 = r.address1,
                            address2 = r.address2,
                            city = r.city,
                            postcode = r.postcode,
                            country = r.country,
                            card_type = r.card_type,
                            expirydate = r.expirydate,
                            cc_no = r.cc_no,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalbillgetdirectdebitaccountOutput outputobj = new UrcrmportalbillgetdirectdebitaccountOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmportalbillgetdirectdebitaccountOutput outputobj = new UrcrmportalbillgetdirectdebitaccountOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
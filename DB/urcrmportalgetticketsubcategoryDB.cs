﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetticketsubcategoryModel;

namespace URCRMDocker.DB
{
    public class urcrmportalgetticketsubcategoryDB
    {
        
        public static List<urcrmportalgetticketsubcategoryOutput> CallDB(urcrmportalgetticketsubcategoryInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetticketsubcategoryOutput> OutputList = new List<urcrmportalgetticketsubcategoryOutput>();
           _logger.LogInformation("Input : urcrmportalgetticketsubcategoryController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_ticket_sub_category";
                    object param = new
                            
                            {
                                @category_id = req.category_id,
                                @sub_category_id = req.sub_category_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetticketsubcategoryOutput()
                        {
                            Sub_category_id = r.Sub_category_id == null ? 0 : r.Sub_category_id,
                            sub_category_name = r.sub_category_name,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetticketsubcategoryOutput outputobj = new urcrmportalgetticketsubcategoryOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetticketsubcategoryOutput outputobj = new urcrmportalgetticketsubcategoryOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}
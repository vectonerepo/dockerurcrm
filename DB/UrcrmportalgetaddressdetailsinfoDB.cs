﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetaddressdetailsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetaddressdetailsinfoDB
    {
        
        public static List<UrcrmportalgetaddressdetailsinfoOutput> CallDB(UrcrmportalgetaddressdetailsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetaddressdetailsinfoOutput> OutputList = new List<UrcrmportalgetaddressdetailsinfoOutput>();
           _logger.LogInformation("Input : urcrmportalcompanydetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_address_details_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @type = req.type
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetaddressdetailsinfoOutput()
                        {
                            streetname = r.streetname,
                            city = r.city,
                            state = r.state,
                            country = r.country,
                            PIN_code = r.PIN_code,
                            user_name = r.user_name,
                            Email_address = r.Email_address,
                            Password = r.Password,
                            address1 = r.address1,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetaddressdetailsinfoOutput outputobj = new UrcrmportalgetaddressdetailsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetaddressdetailsinfoOutput outputobj = new UrcrmportalgetaddressdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
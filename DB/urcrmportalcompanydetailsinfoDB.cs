﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalcompanydetailsinfoModel;

namespace URCRMDocker.DB
{
    public class urcrmportalcompanydetailsinfoDB
    {
        
        public static List<urcrmportalcompanydetailsinfoOutput> CallDB(urcrmportalcompanydetailsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalcompanydetailsinfoOutput> OutputList = new List<urcrmportalcompanydetailsinfoOutput>();
           _logger.LogInformation("Input : urcrmportalcompanydetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_company_details_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalcompanydetailsinfoOutput()
                        {
                            company_id = r.company_id == null ? 0 : r.company_id,
                            company_name = r.company_name,
                            Account_status = r.Account_status,
                            address1 = r.address1,
                            address2 = r.address2,
                            address3 = r.address3,
                            city = r.city,
                            contact_first_name = r.contact_first_name,
                            contact_last_name = r.contact_last_name,
                            contact_phone = r.contact_phone,
                            email = r.email,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalcompanydetailsinfoOutput outputobj = new urcrmportalcompanydetailsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalcompanydetailsinfoOutput outputobj = new urcrmportalcompanydetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
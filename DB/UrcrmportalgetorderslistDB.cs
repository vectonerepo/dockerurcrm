﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetorderslistModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetorderslistDB
    {
        
        public static List<UrcrmportalgetorderslistOutput> CallDB(UrcrmportalgetorderslistInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetorderslistOutput> OutputList = new List<UrcrmportalgetorderslistOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_orders_list";
                object param = new
                        
                        {
                            @company_id = req.company_id,
                            @Subscription_type = req.Subscription_type
                        };
                        
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UrcrmportalgetorderslistOutput()
                    {
                        customer_id = r.customer_id == null ? 0 : r.customer_id,
                        company_name = r.company_name,
                        Orderid = r.Orderid == null ? 0 : r.Orderid,
                        Subscription_date = r.Subscription_date == null ? null : r.Subscription_date,
                        Subscription_type = r.Subscription_type,
                        created_by = r.created_by,
                        order_type = r.order_type,
                        plan_name = r.plan_name,
                        no_of_users = r.no_of_users == null ? 0 : r.no_of_users,
                        Amount_paid = r.Amount_paid == null ? 0.0D : r.Amount_paid,
                        Payment_ref = r.Payment_ref,
                        status = r.status,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UrcrmportalgetorderslistOutput outputobj = new UrcrmportalgetorderslistOutput();                   
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalbillgetserviceplanModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalbillgetserviceplanDB
    {
        
        public static List<UrcrmportalbillgetserviceplanOutput> CallDB(UrcrmportalbillgetserviceplanInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalbillgetserviceplanOutput> OutputList = new List<UrcrmportalbillgetserviceplanOutput>();
           _logger.LogInformation("Input : UrcrmportalbillgetserviceplanController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_bill_get_service_plan";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalbillgetserviceplanOutput()
                        {
                            prd_name = r.prd_name,
                            pln_name = r.pln_name,
                            pc_price = r.pc_price == null ? 0.0D : r.pc_price,
                            price_per_user = r.price_per_user,
                            ord_user_range = r.ord_user_range,
                            account_credit = r.account_credit == null ? 0.0D : r.account_credit,
                            contract_period = r.contract_period,
                            bill_cycle = r.bill_cycle,
                            bill_date = r.bill_date,
                            bill_due_date = r.bill_due_date,
                            bundle_plan = r.bundle_plan,
                            bundle_mins_usage = r.bundle_mins_usage == null ? 0.0D : r.bundle_mins_usage,
                            tot_localno = r.tot_localno == null ? 0 : r.tot_localno,
                            localno_price = r.localno_price == null ? 0.0D : r.localno_price,
                            tot_inter_nat_no = r.tot_inter_nat_no == null ? 0 : r.tot_inter_nat_no,
                            inter_nat_no_price = r.inter_nat_no_price == null ? 0.0D : r.inter_nat_no_price,
                            tot_non_geo_no = r.tot_non_geo_no == null ? 0 : r.tot_non_geo_no,
                            non_geo_no_price = r.non_geo_no_price == null ? 0.0D : r.non_geo_no_price,
                            bill_cycle_type = r.bill_cycle_type,
                            calling_credit = r.calling_credit == null ? 0.0D : r.calling_credit,
                            threshold_limit = r.threshold_limit == null ? 0.0D : r.threshold_limit,
                            Is_free_trail = r.Is_free_trail == null ? 0 : r.Is_free_trail,
                            new_price_combined = r.new_price_combined,
                            Calling_UK_mins = r.Calling_UK_mins == null ? 0.0D : r.Calling_UK_mins,
                            device_phone_count = r.device_phone_count == null ? 0 : r.device_phone_count,
                            Unlimited_inbound_conf = r.Unlimited_inbound_conf == null ? 0 : r.Unlimited_inbound_conf,
                            Unlimited_inbound_conf_value = r.Unlimited_inbound_conf_value == null ? 0.0D : r.Unlimited_inbound_conf_value,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalbillgetserviceplanOutput outputobj = new UrcrmportalbillgetserviceplanOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmportalbillgetserviceplanOutput outputobj = new UrcrmportalbillgetserviceplanOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
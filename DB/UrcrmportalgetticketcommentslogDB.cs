﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetticketcommentslogModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetticketcommentslogDB
    {
        
        public static List<UrcrmportalgetticketcommentslogOutput> CallDB(UrcrmportalgetticketcommentslogInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetticketcommentslogOutput> OutputList = new List<UrcrmportalgetticketcommentslogOutput>();
           _logger.LogInformation("Input : UrcrmportalgetticketcommentslogController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_ticket_comments_log";
                    object param = new
                            
                            {
                                @Ticket_id = req.Ticket_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetticketcommentslogOutput()
                        {
                            id = r.id == null ? 0 : r.id,
                            Ticket_id = r.Ticket_id == null ? 0 : r.Ticket_id,
                            comments = r.comments,
                            create_date = r.create_date == null ? null : r.create_date,                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetticketcommentslogOutput outputobj = new UrcrmportalgetticketcommentslogOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetticketcommentslogOutput outputobj = new UrcrmportalgetticketcommentslogOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
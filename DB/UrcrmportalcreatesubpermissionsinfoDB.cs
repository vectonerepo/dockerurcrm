﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreatesubpermissionsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreatesubpermissionsinfoDB
    {
        
        public static List<UrcrmportalcreatesubpermissionsinfoOutput> CallDB(UrcrmportalcreatesubpermissionsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreatesubpermissionsinfoOutput> OutputList = new List<UrcrmportalcreatesubpermissionsinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcreatesubpermissionsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_sub_permissions_info";
                    object param = new
                            
                            {
                                @sub_id = req.sub_id,
                                @fk_sub_perm_id = req.fk_sub_perm_id,
                                @sub_perm_name = req.sub_perm_name,
                                @sub_perm_status = req.sub_perm_status,
                                @processtype = req.processtype,
                                @sub_perm_link = req.sub_perm_link,
                                @menu_type = req.menu_type
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreatesubpermissionsinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreatesubpermissionsinfoOutput outputobj = new UrcrmportalcreatesubpermissionsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreatesubpermissionsinfoOutput outputobj = new UrcrmportalcreatesubpermissionsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
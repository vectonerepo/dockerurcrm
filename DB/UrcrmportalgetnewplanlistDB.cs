﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetnewplanlistModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetnewplanlistDB
    {

        
        public static List<UrcrmportalgetnewplanlistOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetnewplanlistOutput> OutputList = new List<UrcrmportalgetnewplanlistOutput>();            
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_new_plan_list";
                    object param = new
                            
                            {                               

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetnewplanlistOutput()
                        {
                            Pln_Idx = r.Pln_Idx == null ? 0 : r.Pln_Idx,
                            New_plan = r.New_plan,
                            Pln_Name = r.Pln_Name,
                            pc_price = r.pc_price == null ? 0.0F : r.pc_price,
                            pc_duration = r.pc_duration == null ? 0 : r.pc_duration,
                            yearly_monthly_Duration = r.yearly_monthly_Duration,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetnewplanlistOutput outputobj = new UrcrmportalgetnewplanlistOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetnewplanlistOutput outputobj = new UrcrmportalgetnewplanlistOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
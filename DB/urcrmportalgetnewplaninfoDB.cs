﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetnewplaninfoModel;
namespace URCRMDocker.DB
{
    public class urcrmportalgetnewplaninfoDB
    {
        
        public static List<urcrmportalgetnewplaninfoOutput> CallDB(urcrmportalgetnewplaninfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetnewplaninfoOutput> OutputList = new List<urcrmportalgetnewplaninfoOutput>();
           _logger.LogInformation("Input : urcrmportalgetnewplaninfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_new_plan_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @plan_duration_input = req.plan_duration_input,
                               @no_of_user = req.no_of_user

                        //    plan_duration_input INT,   no_of_user INT 

                    };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetnewplaninfoOutput()
                        {
                            Pln_Idx = r.Pln_Idx == null ? 0 : r.Pln_Idx,
                            New_plan = r.New_plan,
                            Pln_Name = r.Pln_Name,
                            pc_price = r.pc_price ,//== null ? 0.0F : r.pc_price,
                            pc_duration = r.pc_duration == null ? 0 : r.pc_duration,                                                   
                            yearly_monthly_Duration = r.yearly_monthly_Duration,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetnewplaninfoOutput outputobj = new urcrmportalgetnewplaninfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetnewplaninfoOutput outputobj = new urcrmportalgetnewplaninfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
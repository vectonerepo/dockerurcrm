﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanyorderinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanyorderinfoDB
    {
        
        public static List<UrcrmportalgetcompanyorderinfoOutput> CallDB(UrcrmportalgetcompanynumberinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanyorderinfoOutput> OutputList = new List<UrcrmportalgetcompanyorderinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanyorderinfoController:" + JsonConvert.SerializeObject(req));
            try
            {    
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_order_info";
                    object param = new
                            
                            {
                                @invoice_id = req.invoice_id,
                                @company_id  = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanyorderinfoOutput()
                        {
                            invoice_id = r.invoice_id == null ? 0 : r.invoice_id,
                            bill_date = r.bill_date == null ? null : r.bill_date,
                            bill_duration = r.bill_duration,
                            bill_due_date = r.bill_due_date == null ? null : r.bill_due_date,
                            reference_no = r.reference_no,
                            description = r.description,
                            charge = r.charge == null ? 0.0D : r.charge,
                            adjustment = r.adjustment == null ? 0.0D : r.adjustment,
                            tax_percentage = r.tax_percentage == null ? 0.0D : r.tax_percentage,
                            tax_fee = r.tax_fee == null ? 0.0D : r.tax_fee,
                            total_bill = r.total_bill == null ? 0.0D : r.total_bill,
                            total_paid = r.total_paid == null ? 0.0D : r.total_paid,
                            pay_type = r.pay_type,
                            payment_date = r.payment_date == null ? null : r.payment_date,
                            payment_reference = r.payment_reference,
                            comp_name = r.comp_name,
                            comp_regno = r.comp_regno,
                            comp_address = r.comp_address,
                            comp_building = r.comp_building,
                            comp_street = r.comp_street,
                            comp_city = r.comp_city,
                            comp_country = r.comp_country,
                            service_plan = r.service_plan,
                            total_delivery_fee = r.total_delivery_fee == null ? 0.0D : r.total_delivery_fee,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanyorderinfoOutput outputobj = new UrcrmportalgetcompanyorderinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanyorderinfoOutput outputobj = new UrcrmportalgetcompanyorderinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
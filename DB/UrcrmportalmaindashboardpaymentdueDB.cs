﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalmaindashboardpaymentdueModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalmaindashboardpaymentdueDB
    {
        
        public static List<UrcrmportalmaindashboardpaymentdueOutput> CallDB(UrcrmportalmaindashboardpaymentdueInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalmaindashboardpaymentdueOutput> OutputList = new List<UrcrmportalmaindashboardpaymentdueOutput>();
           _logger.LogInformation("Input : UrcrmportalmaindashboardpaymentdueController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_main_dashboard_payment_due";
                    object param = new
                            
                            {
                                @processtype = req.processtype,
                                @From_date = req.From_date,
                                @to_date = req.to_date
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalmaindashboardpaymentdueOutput()
                        {
                            Tot_payment_due = r.Tot_payment_due == null ? 0.0F : r.Tot_payment_due,
                            new_payment_due = r.new_payment_due == null ? 0.0F : r.new_payment_due,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalmaindashboardpaymentdueOutput outputobj = new UrcrmportalmaindashboardpaymentdueOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalmaindashboardpaymentdueOutput outputobj = new UrcrmportalmaindashboardpaymentdueOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
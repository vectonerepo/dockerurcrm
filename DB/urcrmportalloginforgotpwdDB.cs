﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalloginforgotpwdModel;

namespace URCRMDocker.DB
{
    public class urcrmportalloginforgotpwdDB
    {
        
        public static List<urcrmportalloginforgotpwdOutput> CallDB(urcrmportalloginforgotpwdInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalloginforgotpwdOutput> OutputList = new List<urcrmportalloginforgotpwdOutput>();
           _logger.LogInformation("Input : urcrmportalloginforgotpwdController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_login_forgot_pwd";
                    object param = new
                            
                            {
                                @email = req.email,
                                @new_pwd = req.new_pwd,
                                @type = req.type,
                                @log_id = req.log_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalloginforgotpwdOutput()
                        {
                            first_name = r.first_name,
                            pass_word = r.pass_word,
                            userid = r.userid == null ? 0 : r.userid,
                            email = r.email,
                            log_id = r.log_id ==  null  ? 0 : r.log_id,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        })); 
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalloginforgotpwdOutput outputobj = new urcrmportalloginforgotpwdOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalloginforgotpwdOutput outputobj = new urcrmportalloginforgotpwdOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
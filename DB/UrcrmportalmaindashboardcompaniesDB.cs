﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalmaindashboardcompaniesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalmaindashboardcompaniesDB
    {
        
        public static List<UrcrmportalmaindashboardcompaniesOutput> CallDB(UrcrmportalmaindashboardcompaniesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalmaindashboardcompaniesOutput> OutputList = new List<UrcrmportalmaindashboardcompaniesOutput>();
           _logger.LogInformation("Input : UrcrmportalmaindashboardcompaniesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_main_dashboard_companies";
                    object param = new
                            
                            {
                                @processtype = req.processtype,
                                @From_date = req.From_date,
                                @to_date = req.to_date
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalmaindashboardcompaniesOutput()
                        {
                            Total_users = r.Total_users == null ? 0 : r.Total_users,
                            New_users = r.New_users == null ? 0 : r.New_users,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalmaindashboardcompaniesOutput outputobj = new UrcrmportalmaindashboardcompaniesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalmaindashboardcompaniesOutput outputobj = new UrcrmportalmaindashboardcompaniesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
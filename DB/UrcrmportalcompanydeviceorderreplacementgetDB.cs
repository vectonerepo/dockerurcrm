﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcompanydeviceorderreplacementgetModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcompanydeviceorderreplacementgetDB
    {
        
        public static List<UrcrmportalcompanydeviceorderreplacementgetOutput> CallDB(UrcrmportalcompanydeviceorderreplacementgetInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcompanydeviceorderreplacementgetOutput> OutputList = new List<UrcrmportalcompanydeviceorderreplacementgetOutput>();
           _logger.LogInformation("Input : UrcrmportalcompanydeviceorderreplacementgetController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_company_device_order_replacement_get";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcompanydeviceorderreplacementgetOutput()
                        {
                            id = r.id == null ? 0 : r.id,
                            order_id = r.order_id == null ? 0 : r.order_id,
                            prod_code = r.prod_code,
                            prod_name = r.prod_name,
                            prod_model = r.prod_model,
                            rpl_date = r.rpl_date == null ? null : r.rpl_date,
                            rpl_quantity = r.rpl_quantity == null ? 0 : r.rpl_quantity,
                            rpl_reason = r.rpl_reason,
                            rpl_status = r.rpl_status == null ? 0 : r.rpl_status,
                            tracking_no = r.tracking_no,
                            comments = r.comments,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            company_name = r.company_name,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            delivery_comment = r.delivery_comment == null ? "" : r.delivery_comment,
                            order_date = r.order_date == null ? null : r.order_date,
                            referenceid = r.referenceid,
                            status_desc = r.status_desc,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcompanydeviceorderreplacementgetOutput outputobj = new UrcrmportalcompanydeviceorderreplacementgetOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcompanydeviceorderreplacementgetOutput outputobj = new UrcrmportalcompanydeviceorderreplacementgetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetprodcutuserinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetprodcutuserinfoDB
    {
        
        public static List<UrcrmportalgetprodcutuserinfoOutput> CallDB(UrcrmportalgetprodcutuserinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetprodcutuserinfoOutput> OutputList = new List<UrcrmportalgetprodcutuserinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetprodcutuserinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_prodcut_user_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @search_mode = req.search_mode
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetprodcutuserinfoOutput()
                        {

                            Firstname = r.Firstname,
                            Email = r.Email,
                            device_type = r.device_type,
                            browser = r.browser,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetprodcutuserinfoOutput outputobj = new UrcrmportalgetprodcutuserinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetprodcutuserinfoOutput outputobj = new UrcrmportalgetprodcutuserinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
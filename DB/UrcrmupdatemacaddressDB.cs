﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmupdatemacaddressModel;

namespace URCRMDocker.DB
{
    public class UrcrmupdatemacaddressDB
    {
        
        public static List<UrcrmupdatemacaddressOutput> CallDB(UrcrmupdatemacaddressInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmupdatemacaddressOutput> OutputList = new List<UrcrmupdatemacaddressOutput>();
           _logger.LogInformation("Input : UrcrmupdatemacaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_update_mac_address";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id,
                                @mac_address = req.mac_address,
                                @referenceid = req.referenceid
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmupdatemacaddressOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmupdatemacaddressOutput outputobj = new UrcrmupdatemacaddressOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmupdatemacaddressOutput outputobj = new UrcrmupdatemacaddressOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
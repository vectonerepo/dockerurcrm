﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetticketPriorityModel;

namespace URCRMDocker.DB
{
    public class urcrmportalgetticketPriorityDB
    {
        
        public static List<urcrmportalgetticketPriorityOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetticketPriorityOutput> OutputList = new List<urcrmportalgetticketPriorityOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_ticket_Priority";
                object param = new
                        
                        {
                        };
                       
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new urcrmportalgetticketPriorityOutput()
                    {
                        Priority_id = r.Priority_id == null ? 0 : r.Priority_id,
                        Priority = r.Priority,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    urcrmportalgetticketPriorityOutput outputobj = new urcrmportalgetticketPriorityOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
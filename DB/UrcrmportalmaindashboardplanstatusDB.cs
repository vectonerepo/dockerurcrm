﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalmaindashboardplanstatusModel;
using MySql.Data.MySqlClient;

namespace URCRMDocker.DB
{
    public class UrcrmportalmaindashboardplanstatusDB
    {

        public static UrcrmportalmaindashboardplanstatusOutput CallDB(UrcrmportalmaindashboardplanstatusInput req, IConfiguration _Config, ILogger _logger)
        {
            _logger.LogInformation("Input : UrcrmportalmaindashboardplanstatusController:" + JsonConvert.SerializeObject(req));

            string connectionType = _Config["Connectiontype:value"];

            if (connectionType.ToLower() == "mysql")
            {

                try
                {
                    using (var conn = new MySqlConnection(_Config["ConnectionStrings:urcrm"]))
                    {
                        conn.Open();
                        var sp = "ur_crm_portal_main_dashboard_plan_status";
                        var result = conn.QueryMultiple(
                                sp, new
                                {
                                    @processtype = req.processtype,
                                    @From_date = req.From_date,
                                    @to_date = req.to_date
                                },
                                commandType: CommandType.StoredProcedure);
                        if (result != null)
                        {


                            UrcrmportalmaindashboardplanstatusOutput output = new UrcrmportalmaindashboardplanstatusOutput();
                            output.Code = 0;
                            output.Message = "Success";

                            output.PlanDetails = (List<PlanDetails>)result.Read<PlanDetails>();
                            output.DateDetails = (List<DateDetails>)result.Read<DateDetails>();
                            return output;
                        }
                        else
                        {

                            UrcrmportalmaindashboardplanstatusOutput outputobj = new UrcrmportalmaindashboardplanstatusOutput();
                            outputobj.Code = -1;
                            outputobj.Message = "No Rec found";
                            return outputobj;
                        }
                    }
                }
                catch (Exception ex)
                {

                    UrcrmportalmaindashboardplanstatusOutput outputobj = new UrcrmportalmaindashboardplanstatusOutput();
                    outputobj.Code = -1;
                    outputobj.Message = ex.Message;
                    return outputobj;
                }
            }
            else
            {
                try
                {
                    using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                    {
                        conn.Open();
                        var sp = "ur_crm_portal_main_dashboard_plan_status";
                        var result = conn.QueryMultiple(
                                sp, new
                                {
                                    @processtype = req.processtype,
                                    @From_date = req.From_date,
                                    @to_date = req.to_date
                                },
                                commandType: CommandType.StoredProcedure);
                        if (result != null)
                        {


                            UrcrmportalmaindashboardplanstatusOutput output = new UrcrmportalmaindashboardplanstatusOutput();
                            output.Code = 0;
                            output.Message = "Success";

                            output.PlanDetails = (List<PlanDetails>)result.Read<PlanDetails>();
                            output.DateDetails = (List<DateDetails>)result.Read<DateDetails>();
                            return output;
                        }
                        else
                        {

                            UrcrmportalmaindashboardplanstatusOutput outputobj = new UrcrmportalmaindashboardplanstatusOutput();
                            outputobj.Code = -1;
                            outputobj.Message = "No Rec found";
                            return outputobj;
                        }
                    }
                }
                catch (Exception ex)
                {

                    UrcrmportalmaindashboardplanstatusOutput outputobj = new UrcrmportalmaindashboardplanstatusOutput();
                    outputobj.Code = -1;
                    outputobj.Message = ex.Message;
                    return outputobj;
                }

            }
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetorderslistfailedModel;
namespace URCRMDocker.DB
{
    public class UrcrmportalgetorderslistfailedDB
    {

        
        public static List<UrcrmportalgetorderslistfailedOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetorderslistfailedOutput> OutputList = new List<UrcrmportalgetorderslistfailedOutput>();
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_orders_list_failed";
                    object param = new
                            
                            {

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetorderslistfailedOutput()
                        {
                            mobileno = r.mobileno,
                            REFERENCE_ID = r.REFERENCE_ID,
                            Pay_decision = r.Pay_decision,
                            Pay_description = r.Pay_description,
                            created_date = r.created_date == null ? null : r.created_date,                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetorderslistfailedOutput outputobj = new UrcrmportalgetorderslistfailedOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetorderslistfailedOutput outputobj = new UrcrmportalgetorderslistfailedOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetcompanyactivitiesinfoModel;
namespace URCRMDocker.DB
{
    public class urcrmportalgetcompanyactivitiesinfoDB
    {
        
        public static List<urcrmportalgetcompanyactivitiesinfoOutput> CallDB(urcrmportalgetcompanyactivitiesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetcompanyactivitiesinfoOutput> OutputList = new List<urcrmportalgetcompanyactivitiesinfoOutput>();
           _logger.LogInformation("Input : urcrmportalgetuserbydeptController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_activities_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetcompanyactivitiesinfoOutput()
                        {
                            cp_id = r.userid == null ? 0 : r.cp_id,
                            plan_Subscription_date = r.plan_Subscription_date == null ? null : r.plan_Subscription_date,
                            plan_changes = r.plan_changes,
                            billing_cycle_changes = r.billing_cycle_changes,
                            minutes_usages = r.minutes_usages == null ? null : r.minutes_usages,
                            extra_minutes_usages = r.extra_minutes_usages == null ? 0.0F : r.extra_minutes_usages,
                            international_calling = r.international_calling == null ? 0.0F : r.international_calling,
                            bundle_subcription = r.bundle_subcription == null ? 0.0F : r.bundle_subcription,
                            bundle_name = r.bundle_name,
                            Activity = r.Activity,
                            activity_done_by = r.activity_done_by,
                            status = r.status,
                            Activity_time = r.Activity_time == null ? null : r.Activity_time,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetcompanyactivitiesinfoOutput outputobj = new urcrmportalgetcompanyactivitiesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetcompanyactivitiesinfoOutput outputobj = new urcrmportalgetcompanyactivitiesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdatecompanydevicesinfoModel;
namespace URCRMDocker.DB
{
    public class UrcrmportalupdatecompanydevicesinfoDB
    {

        
        public static List<UrcrmportalupdatecompanydevicesinfoOutput> CallDB(UrcrmportalupdatecompanydevicesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdatecompanydevicesinfoOutput> OutputList = new List<UrcrmportalupdatecompanydevicesinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalupdatecompanydevicesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_company_devices_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id,
                                @device_approval = req.device_approval,
                                @mac_address = req.mac_address,
                                @category = req.category,
                                @tracking_no = req.tracking_no,
                                @delivery_date = req.delivery_date,
                                @delivery_comment = req.delivery_comment,
                                @comment = req.comment

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdatecompanydevicesinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdatecompanydevicesinfoOutput outputobj = new UrcrmportalupdatecompanydevicesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdatecompanydevicesinfoOutput outputobj = new UrcrmportalupdatecompanydevicesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
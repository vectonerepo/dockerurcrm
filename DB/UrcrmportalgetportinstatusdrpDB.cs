﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetportinstatusdrpModel;
using URCRMDocker.Controllers;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetportinstatusdrpDB
    {
        
        public static List<UrcrmportalgetportinstatusdrpOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetportinstatusdrpOutput> OutputList = new List<UrcrmportalgetportinstatusdrpOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_portin_status_drp";
                object param = new
                        
                        {
                        };
                        
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UrcrmportalgetportinstatusdrpOutput()
                    {
                        status_id = r.status_id == null ? 0 : r.status_id,
                        status_desc = r.status_desc,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UrcrmportalgetportinstatusdrpOutput outputobj = new UrcrmportalgetportinstatusdrpOutput();
                    outputobj.Code = 1003;
                    outputobj.Message = ErrorMessage.Errors[1003];
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
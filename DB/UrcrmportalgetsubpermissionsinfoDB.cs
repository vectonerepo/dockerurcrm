﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetsubpermissionsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetsubpermissionsinfoDB
    {
        
        public static List<UrcrmportalgetsubpermissionsinfoOutput> CallDB(UrcrmportalgetsubpermissionsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetsubpermissionsinfoOutput> OutputList = new List<UrcrmportalgetsubpermissionsinfoOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_sub_permissions_info";
                object param = new
                        
                        {
                            @sub_perm_id = req.sub_perm_id
                        };
                       
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UrcrmportalgetsubpermissionsinfoOutput()
                    {
                        sub_perm_id = r.sub_perm_id == null ? 0 : r.sub_perm_id,
                        fk_sub_perm_id = r.fk_sub_perm_id == null ? 0 : r.fk_sub_perm_id,
                        sub_perm_name = r.sub_perm_name,
                        sub_perm_status = r.sub_perm_status == null ? 0 : r.sub_perm_status,
                        sub_perm_link = r.sub_perm_link,
                        permission_name = r.permission_name,
                        menu_type = r.menu_type,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UrcrmportalgetsubpermissionsinfoOutput outputobj = new UrcrmportalgetsubpermissionsinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
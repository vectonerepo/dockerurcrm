﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanydashboardinfoModel;
using MySql.Data.MySqlClient;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanydashboardinfoDB
    {


        public static UrcrmportalgetcompanydashboardinfoOutput CallDB(UrcrmportalgetcompanydashboardinfoInput req, IConfiguration _Config, ILogger _logger)
        {
            _logger.LogInformation("Input : UrcrmportalgetcompanydashboardinfoController:" + JsonConvert.SerializeObject(req));
            //    try
            //    {
            //        // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            //        {
            //            // conn.Open();
            //            var sp = "ur_crm_portal_get_company_dashboard_info";
            //            object param = new
            //            {
            //                @company_id = req.company_id
            //            };

            //            SqlMapper.GridReader result = null; DBConnection.CheckDB.CallDBConnectionQueryMultiple(_Config, sp, param, ref result, "ConnectionStrings:urcrm");


            //            if (result != null)
            //            {
            //                _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
            //                UrcrmportalgetcompanydashboardinfoOutput output = new UrcrmportalgetcompanydashboardinfoOutput();
            //                output.Code = 0;
            //                output.Message = "Success";
            //                output.companyDetails = (List<companyDetails>)result.Read<companyDetails>();
            //                output.UserDetails = (List<UserDetails>)result.Read<UserDetails>();
            //                output.DidDeatils = (List<DidDeatils>)result.Read<DidDeatils>();
            //                output.Countrydeatils = (List<Countrydeatils>)result.Read<Countrydeatils>();

            //                return output;
            //            }
            //            else
            //            {
            //                _logger.LogInformation("Output DB : " + "Empty result");
            //                UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
            //                outputobj.Code = -1;
            //                outputobj.Message = "No Rec found";
            //                return outputobj;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        //Log.Error(ex.Message);
            //        UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
            //        outputobj.Code = -1;
            //        outputobj.Message = ex.Message;
            //        return outputobj;
            //    }

            string connectionType = _Config["Connectiontype:value"];

            if (connectionType.ToLower() == "mysql")
            {

                try
                {

                    using (var conn = new MySqlConnection(_Config["ConnectionStrings:urcrm"]))
                    {
                        conn.Open();
                        var sp = "ur_crm_portal_get_company_dashboard_info";
                        var result = conn.QueryMultiple(
                                sp, new
                                {
                                    @company_id = req.company_id
                                },
                                commandType: CommandType.StoredProcedure);
                        if (result != null)
                        {

                            UrcrmportalgetcompanydashboardinfoOutput output = new UrcrmportalgetcompanydashboardinfoOutput();
                            output.Code = 0;
                            output.Message = "Success";
                            output.companyDetails = (List<companyDetails>)result.Read<companyDetails>();
                            output.UserDetails = (List<UserDetails>)result.Read<UserDetails>();
                            output.DidDeatils = (List<DidDeatils>)result.Read<DidDeatils>();
                            output.Countrydeatils = (List<Countrydeatils>)result.Read<Countrydeatils>();

                            return output;
                        }
                        else
                        {

                            UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
                            outputobj.Code = -1;
                            outputobj.Message = "No Rec found";
                            return outputobj;
                        }
                    }
                }
                catch (Exception ex)
                {

                    UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = ex.Message;
                    return outputobj;
                }
            }
            else
            {
                try
                {

                    using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                    {
                        conn.Open();
                        var sp = "ur_crm_portal_get_company_dashboard_info";
                        var result = conn.QueryMultiple(
                                sp, new
                                {
                                    @company_id = req.company_id
                                },
                                commandType: CommandType.StoredProcedure);
                        if (result != null)
                        {

                            UrcrmportalgetcompanydashboardinfoOutput output = new UrcrmportalgetcompanydashboardinfoOutput();
                            output.Code = 0;
                            output.Message = "Success";
                            output.companyDetails = (List<companyDetails>)result.Read<companyDetails>();
                            output.UserDetails = (List<UserDetails>)result.Read<UserDetails>();
                            output.DidDeatils = (List<DidDeatils>)result.Read<DidDeatils>();
                            output.Countrydeatils = (List<Countrydeatils>)result.Read<Countrydeatils>();

                            return output;
                        }
                        else
                        {

                            UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
                            outputobj.Code = -1;
                            outputobj.Message = "No Rec found";
                            return outputobj;
                        }
                    }
                }
                catch (Exception ex)
                {

                    UrcrmportalgetcompanydashboardinfoOutput outputobj = new UrcrmportalgetcompanydashboardinfoOutput();
                    outputobj.Code = -1;
                    outputobj.Message = ex.Message;
                    return outputobj;
                }
            }
        }
    }
}
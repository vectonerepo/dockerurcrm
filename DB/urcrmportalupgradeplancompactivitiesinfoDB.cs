﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalupgradeplancompactivitiesinfoModel;

namespace URCRMDocker.DB
{
    public class urcrmportalupgradeplancompactivitiesinfoDB
    {
        
        public static List<urcrmportalupgradeplancompactivitiesinfoOutput> CallDB(urcrmportalupgradeplancompactivitiesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalupgradeplancompactivitiesinfoOutput> OutputList = new List<urcrmportalupgradeplancompactivitiesinfoOutput>();
           _logger.LogInformation("Input : urcrmportalupgradeplancompactivitiesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_upgrade_plan_comp_activities_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @plan_name = req.plan_name, 
                                @plan_price = req.plan_price,
                                @plan_duration = req.plan_duration, 
                                @processtype = req.processtype,
                                @plan_subscribe = req.plan_subscribe,
                                @plan_id =  req.plan_id,
                                @product_id = req.product_id,
                                @reference_no = req.reference_no,
                                @plan_description = req.plan_description,
                                @adjustment_charge = req.adjustment_charge,
                                @tax_fee = req.tax_fee,
                                @tax_per = req.tax_per,
                                @total_plan_charge = req.total_plan_charge,
                                @userid = req.userid
                            };
                            //@product_id int
                            //@reference_no   varchar
                            //@plan_description   varchar
                            //@adjustment_charge  float
                            //@tax_fee    float
                            //@tax_per    int
                            //@total_plan_charge  float
                            //@userid int

                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalupgradeplancompactivitiesinfoOutput()
                        {
                            plan_name =r.plan_name,
                            price = r.price == null ? 0.0F : r.price,
                            subscription_type =r.subscription_type,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalupgradeplancompactivitiesinfoOutput outputobj = new urcrmportalupgradeplancompactivitiesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalupgradeplancompactivitiesinfoOutput outputobj = new urcrmportalupgradeplancompactivitiesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
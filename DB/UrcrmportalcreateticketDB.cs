﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreateticketModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreateticketDB
    {

        
        public static List<UrcrmportalcreateticketOutput> CallDB(UrcrmportalcreateticketInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreateticketOutput> OutputList = new List<UrcrmportalcreateticketOutput>();
           _logger.LogInformation("Input : UrcrmportalcreateticketController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_ticket";
                    object param = new
                            
                            {
                                @Title = req.Title,
                                @desc = req.desc,
                                @Fk_category_id = req.Fk_category_id,
                                @Fk_Sub_category_id = req.Fk_Sub_category_id,
                                @Fk_Status = req.Fk_Status,
                                @Fk_priorty_id = req.Fk_priorty_id,
                                @Fk_User_id = req.Fk_User_id,
                                @create_by = req.create_by,
                                @attachments = req.attachments,
                                @process_type = req.process_type,
                                @Ticket_id = req.Ticket_id,
                                @company_id = req.company_id,
                                @comments = req.comments
                        //--Ticket_id INT = 0, company_id int  =0, comments longtext =’’
                    };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreateticketOutput()
                        {
                            Ticket_id = r.Ticket_id,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreateticketOutput outputobj = new UrcrmportalcreateticketOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreateticketOutput outputobj = new UrcrmportalcreateticketOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}
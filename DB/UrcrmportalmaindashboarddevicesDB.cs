﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalmaindashboarddevicesModel;
namespace URCRMDocker.DB
{
    public class UrcrmportalmaindashboarddevicesDB
    {
        
        public static List<UrcrmportalmaindashboarddevicesOutput> CallDB(UrcrmportalmaindashboarddevicesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalmaindashboarddevicesOutput> OutputList = new List<UrcrmportalmaindashboarddevicesOutput>();
           _logger.LogInformation("Input : UrcrmportalmaindashboarddevicesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_main_dashboard_devices";
                    object param = new
                            
                            {
                                @processtype = req.processtype,
                                @From_date = req.From_date,
                                @to_date = req.to_date
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalmaindashboarddevicesOutput()
                        {
                            Total_devices = r.Total_devices == null ? 0 : r.Total_devices,
                            New_devices = r.New_devices == null ? 0 : r.New_devices,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalmaindashboarddevicesOutput outputobj = new UrcrmportalmaindashboarddevicesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalmaindashboarddevicesOutput outputobj = new UrcrmportalmaindashboarddevicesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
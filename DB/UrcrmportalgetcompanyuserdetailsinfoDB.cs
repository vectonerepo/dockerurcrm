﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanyuserdetailsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanyuserdetailsinfoDB
    {
        
        public static List<UrcrmportalgetcompanyuserdetailsinfoOutput> CallDB(UrcrmportalgetcompanyuserdetailsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanyuserdetailsinfoOutput> OutputList = new List<UrcrmportalgetcompanyuserdetailsinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalgetcompanyuserdetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_user_details_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @extension = req.extension,
                                @processtype = req.processtype,
                                @User_id = req.User_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanyuserdetailsinfoOutput()
                        {
                            user = r.user,
                            user_type = r.user_type,
                            created_on = r.created_on == null ? null : r.created_on,
                            created_by = r.created_by,
                            ext_no = r.ext_no, /*== null ? 0 : r.ext_no,*/
                            did_number = r.did_number,
                            location = r.location,
                            status = r.status,
                            surname = r.surname,
                            userid = r.userid == null ? 0 : r.userid,
                            assign = r.assign == null ? 0 : r.assign,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Role_name = r.Role_name,
                            main_number = r.main_number,
                            conference_number =  r.conference_number,
                            assigned_phone_number = r.assigned_phone_number,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanyuserdetailsinfoOutput outputobj = new UrcrmportalgetcompanyuserdetailsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmportalgetcompanyuserdetailsinfoOutput outputobj = new UrcrmportalgetcompanyuserdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
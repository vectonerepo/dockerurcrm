﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmRoleMenuMappinggetinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmRoleMenuMappinggetinfoDB
    {
        
        public static List<UrcrmRoleMenuMappinggetinfoOutput> CallDB(UrcrmRoleMenuMappinggetinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmRoleMenuMappinggetinfoOutput> OutputList = new List<UrcrmRoleMenuMappinggetinfoOutput>();
           _logger.LogInformation("Input : UrcrmRoleMenuMappinggetinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_Role_Menu_Mapping_get_info";
                    object param = new
                            
                            {
                                @Role_ID = req.Role_ID,
                                @sub_perm_id = req.sub_perm_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmRoleMenuMappinggetinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmRoleMenuMappinggetinfoOutput outputobj = new UrcrmRoleMenuMappinggetinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmRoleMenuMappinggetinfoOutput outputobj = new UrcrmRoleMenuMappinggetinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
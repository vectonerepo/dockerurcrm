﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreatecompanynotesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreatecompanynotesDB
    {
        
        public static List<UrcrmportalcreatecompanynotesOutput> CallDB(UrcrmportalcreatecompanynotesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreatecompanynotesOutput> OutputList = new List<UrcrmportalcreatecompanynotesOutput>();
           _logger.LogInformation("Input : UrcrmportalcreatecompanynotesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_company_notes";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @notes_subject = req.notes_subject,
                                @Notes_description = req.Notes_description,
                                @processtype = req.processtype,
                                @notes_id = req.notes_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreatecompanynotesOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreatecompanynotesOutput outputobj = new UrcrmportalcreatecompanynotesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreatecompanynotesOutput outputobj = new UrcrmportalcreatecompanynotesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
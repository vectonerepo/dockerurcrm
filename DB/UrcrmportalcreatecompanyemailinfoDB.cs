﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreatecompanyemailinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreatecompanyemailinfoDB
    {
        
        public static List<UrcrmportalcreatecompanyemailinfoOutput> CallDB(UrcrmportalcreatecompanyemailinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreatecompanyemailinfoOutput> OutputList = new List<UrcrmportalcreatecompanyemailinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcreatecompanyemailinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_company_email_info";
                    object param = new
                            
                            {
                                @email_id  = req.email_id,
                                @company_id = req.company_id,
                                @email_from = req.email_from,
                                @email_to = req.email_to,
                                @email_subject = req.email_subject,
                                @email_body = req.email_body,
                                @no_of_attachaments = req.no_of_attachaments,
                                @attachment_location = req.attachment_location,
                                @create_by = req.create_by,
                                @processtype = req.processtype
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreatecompanyemailinfoOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreatecompanyemailinfoOutput outputobj = new UrcrmportalcreatecompanyemailinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreatecompanyemailinfoOutput outputobj = new UrcrmportalcreatecompanyemailinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
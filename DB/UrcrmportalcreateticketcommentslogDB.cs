﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcreateticketcommentslogModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcreateticketcommentslogDB
    {
        
        public static List<UrcrmportalcreateticketcommentslogOutput> CallDB(UrcrmportalcreateticketcommentslogInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcreateticketcommentslogOutput> OutputList = new List<UrcrmportalcreateticketcommentslogOutput>();
           _logger.LogInformation("Input : UrcrmportalcreateticketcommentslogController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_create_ticket_comments_log";
                    object param = new
                            
                            {
                                @Ticket_id = req.Ticket_id,
                                @comments = req.comments
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcreateticketcommentslogOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcreateticketcommentslogOutput outputobj = new UrcrmportalcreateticketcommentslogOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcreateticketcommentslogOutput outputobj = new UrcrmportalcreateticketcommentslogOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
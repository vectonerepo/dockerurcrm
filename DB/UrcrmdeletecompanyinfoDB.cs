﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmdeletecompanyinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmdeletecompanyinfoDB
    {
        
        public static List<UrcrmdeletecompanyinfoOutput> CallDB(UrcrmdeletecompanyinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmdeletecompanyinfoOutput> OutputList = new List<UrcrmdeletecompanyinfoOutput>();
           _logger.LogInformation("Input : urcrmportalloginforgotpwdController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_delete_company_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmdeletecompanyinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,

                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmdeletecompanyinfoOutput outputobj = new UrcrmdeletecompanyinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmdeletecompanyinfoOutput outputobj = new UrcrmdeletecompanyinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetconfigurationrolesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetconfigurationrolesDB
    {
        
        public static List<UrcrmportalgetconfigurationrolesOutput> CallDB(UrcrmportalgetconfigurationrolesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetconfigurationrolesOutput> OutputList = new List<UrcrmportalgetconfigurationrolesOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_configuration_roles";
                object param = new
                        
                        {
                            @role_id = req.role_id,
                        };
                       
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new UrcrmportalgetconfigurationrolesOutput()
                    {
                        Role_name = r.Role_name,
                        Role_type = r.Role_type,
                        Description = r.Description,
                        Role_status = r.Role_status == null ? 0 : r.Role_status,
                        Role_id= r.Role_id,
                        Roles_permission = r.Roles_permission,
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    UrcrmportalgetconfigurationrolesOutput outputobj = new UrcrmportalgetconfigurationrolesOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }
    }
}
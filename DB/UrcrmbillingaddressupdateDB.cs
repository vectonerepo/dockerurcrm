﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmbillingaddressupdateModel;

namespace URCRMDocker.DB
{
    public class UrcrmbillingaddressupdateDB
    {      
        public static List<UrcrmbillingaddressupdateOutput> CallDB(UrcrmbillingaddressupdateInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmbillingaddressupdateOutput> OutputList = new List<UrcrmbillingaddressupdateOutput>();
           _logger.LogInformation("Input : UrcrmbillingaddressupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_billing_address_update";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @address1 = req.address1,
                                @address2 = req.address2,
                                @city = req.city,
                                @postcode = req.postcode,
                                @country = req.country,
                                @process_type = req.process_type
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmbillingaddressupdateOutput()
                        {
                            address1 = r.address1,
                            address2 = r.address2,
                            city = r.city,
                            postcode = r.postcode,
                            country = r.country,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmbillingaddressupdateOutput outputobj = new UrcrmbillingaddressupdateOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmbillingaddressupdateOutput outputobj = new UrcrmbillingaddressupdateOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
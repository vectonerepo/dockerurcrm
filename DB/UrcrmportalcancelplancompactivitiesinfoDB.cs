﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcancelplancompactivitiesinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcancelplancompactivitiesinfoDB
    {
        
        public static List<UrcrmportalcancelplancompactivitiesinfoOutput> CallDB(UrcrmportalcancelplancompactivitiesinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcancelplancompactivitiesinfoOutput> OutputList = new List<UrcrmportalcancelplancompactivitiesinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcancelplancompactivitiesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    //UTMS_create_plan_info_test_check
                    var sp = "ur_crm_portal_cancel_plan_comp_activities_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @userid = req.userid
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcancelplancompactivitiesinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcancelplancompactivitiesinfoOutput outputobj = new UrcrmportalcancelplancompactivitiesinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcancelplancompactivitiesinfoOutput outputobj = new UrcrmportalcancelplancompactivitiesinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
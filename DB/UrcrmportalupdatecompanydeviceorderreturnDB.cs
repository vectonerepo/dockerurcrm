﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdatecompanydeviceorderreturnModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalupdatecompanydeviceorderreturnDB
    {
        
        public static List<UrcrmportalupdatecompanydeviceorderreturnOutput> CallDB(UrcrmportalupdatecompanydeviceorderreturnInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdatecompanydeviceorderreturnOutput> OutputList = new List<UrcrmportalupdatecompanydeviceorderreturnOutput>();
           _logger.LogInformation("Input : UrcrmportalupdatecompanydeviceorderreturnController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_company_device_order_return";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id,
                                @device_approval = req.device_approval,
                                @tracking_no = req.tracking_no,
                                @comments = req.comments,
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdatecompanydeviceorderreturnOutput()
                        {
                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdatecompanydeviceorderreturnOutput outputobj = new UrcrmportalupdatecompanydeviceorderreturnOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdatecompanydeviceorderreturnOutput outputobj = new UrcrmportalupdatecompanydeviceorderreturnOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
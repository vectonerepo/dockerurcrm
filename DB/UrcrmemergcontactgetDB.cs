﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmemergcontactgetModel;

namespace URCRMDocker.DB
{
    public class UrcrmemergcontactgetDB
    {

        public static List<UrcrmemergcontactgetOutput> CallDB(UrcrmemergcontactgetInput req, IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmemergcontactgetOutput> OutputList = new List<UrcrmemergcontactgetOutput>();
            _logger.LogInformation("Input : UrcrmemergcontactgetController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                    // conn.Open();
                    var sp = "Ur_crm_emerg_contact_get";
                    object param = new

                    {
                        @company_id = req.company_id

                    };

                    IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");
                    if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmemergcontactgetOutput()
                        {
                            post_code = r.post_code,
                            address_line_1 = r.address_line_1,
                            address_line_2 = r.address_line_2,
                            address_line_3 = r.address_line_3,
                            city = r.city,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmemergcontactgetOutput outputobj = new UrcrmemergcontactgetOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmemergcontactgetOutput outputobj = new UrcrmemergcontactgetOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
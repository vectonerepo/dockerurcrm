﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalcustomizationuserinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalcustomizationuserinfoDB
    {
        
        public static List<UrcrmportalcustomizationuserinfoOutput> CallDB(UrcrmportalcustomizationuserinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalcustomizationuserinfoOutput> OutputList = new List<UrcrmportalcustomizationuserinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalcustomizationuserinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_customization_user_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @processtype = req.processtype,
                                @ID = req.ID
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalcustomizationuserinfoOutput()
                        {
                            username = r.username,
                            number = r.number,
                            Extension_Number = r.Extension_Number,
                            Roles = r.Roles,
                            company = r.company,
                            Department = r.Department,
                            Status = r.Status,
                            ID = r.ID == null ? 0 : r.ID,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalcustomizationuserinfoOutput outputobj = new UrcrmportalcustomizationuserinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalcustomizationuserinfoOutput outputobj = new UrcrmportalcustomizationuserinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
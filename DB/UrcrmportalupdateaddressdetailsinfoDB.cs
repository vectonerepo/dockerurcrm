﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdateaddressdetailsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalupdateaddressdetailsinfoDB
    {
        
        public static List<UrcrmportalupdateaddressdetailsinfoOutput> CallDB(UrcrmportalupdateaddressdetailsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdateaddressdetailsinfoOutput> OutputList = new List<UrcrmportalupdateaddressdetailsinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalupdateaddressdetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_address_details_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @streetname = req.streetname,
                                @city = req.city,
                                @state = req.state,
                                @country = req.country,
                                @PIN_code = req.PIN_code,
                                @processtype = req.processtype,
                                @address1 = req.address1
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdateaddressdetailsinfoOutput()
                        {                            
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdateaddressdetailsinfoOutput outputobj = new UrcrmportalupdateaddressdetailsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdateaddressdetailsinfoOutput outputobj = new UrcrmportalupdateaddressdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
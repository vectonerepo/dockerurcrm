﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportaluserloginModel;

namespace URCRMDocker.DB
{
    public class urcrmportaluserloginDB
    {
        
        public static List<urcrmportaluserloginOutput> CallDB(urcrmportaluserloginInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportaluserloginOutput> OutputList = new List<urcrmportaluserloginOutput>();
           _logger.LogInformation("Input : urcrmportaluserloginController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
        var sp = "ur_crm_portal_user_login";
                    object param = new
                            
                            {
                                @username = req.username,
                                @password = req.password

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportaluserloginOutput()
                        {
                            user_role = r.user_role == null ? 0 : r.user_role,
                            userid = r.userid == null ? 0 : r.userid,
                            username = r.username,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportaluserloginOutput outputobj = new urcrmportaluserloginOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportaluserloginOutput outputobj = new urcrmportaluserloginOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetticketstatusModel;

namespace URCRMDocker.DB
{
    public class urcrmportalgetticketstatusDB
    {
        
        public static List<urcrmportalgetticketstatusOutput> CallDB(IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetticketstatusOutput> OutputList = new List<urcrmportalgetticketstatusOutput>();

           // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
            {
              // conn.Open();
                var sp = "ur_crm_portal_get_ticket_status";
                object param = new
                        
                        {
                        };
                       
                IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                {
                    OutputList.AddRange(result.Select(r => new urcrmportalgetticketstatusOutput()
                    {
                        Status_id = r.Status_id == null ? 0 : r.Status_id,
                        Status_name = r.Status_name,                  
                        Code = r.errcode == null ? 0 : r.errcode,
                        Message = r.errmsg == null ? "Success" : r.errmsg,
                    }));
                }
                else
                {
                    urcrmportalgetticketstatusOutput outputobj = new urcrmportalgetticketstatusOutput();
                    outputobj.Code = -1;
                    outputobj.Message = "No Rec found";
                    OutputList.Add(outputobj);
                }
               _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(OutputList));
                return OutputList;
            }
        }

    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmemergcontactupdateModel;

namespace URCRMDocker.DB
{
    public class UrcrmemergcontactupdateDB
    {
        
        public static List<UrcrmemergcontactupdateOutput> CallDB(UrcrmemergcontactupdateInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmemergcontactupdateOutput> OutputList = new List<UrcrmemergcontactupdateOutput>();
           _logger.LogInformation("Input : UrcrmemergcontactupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_emerg_contact_update";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @post_code = req.post_code,
                                @address_line_1= req.address_line_1,
                                @address_line_2 = req.address_line_2,
                                @address_line_3 = req.address_line_3,
                                @city = req.city
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmemergcontactupdateOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmemergcontactupdateOutput outputobj = new UrcrmemergcontactupdateOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmemergcontactupdateOutput outputobj = new UrcrmemergcontactupdateOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
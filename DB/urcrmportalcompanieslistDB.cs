﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalcompanieslistModel;

namespace URCRMDocker.DB
{
    public class urcrmportalcompanieslistDB
    {
        
        public static List<urcrmportalcompanieslistOutput> CallDB(urcrmportalcompanieslistInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalcompanieslistOutput> OutputList = new List<urcrmportalcompanieslistOutput>();
           _logger.LogInformation("Input : urcrmportalloginforgotpwdController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_companies_list";
                    object param = new
                            
                            {
                                @From_date = req.From_date,
                                @to_date = req.to_date
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalcompanieslistOutput()
                        {
                            company_id = r.company_id == null ? 0 : r.company_id,
                            comp_name = r.comp_name,
                            Pln_Name = r.Pln_Name,
                            Subscription_type = r.Subscription_type,
                            Subscription_date  = r.Subscription_date == null ? null : r.Subscription_date,
                            Location = r.Location,
                            status = r.status,
                            email = r.email,
                            Comp_is_free_trail = r.Comp_is_free_trail,
                            plan_end_date = r.plan_end_date == null ? null : r.plan_end_date,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg,
                            create_by = r.create_by,
                            create_from = r.create_from
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalcompanieslistOutput outputobj = new urcrmportalcompanieslistOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalcompanieslistOutput outputobj = new urcrmportalcompanieslistOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
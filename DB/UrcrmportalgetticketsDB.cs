﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetticketsModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetticketsDB
    {
        
        public static List<UrcrmportalgetticketsOutput> CallDB(UrcrmportalgetticketsInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetticketsOutput> OutputList = new List<UrcrmportalgetticketsOutput>();
           _logger.LogInformation("Input : UrcrmportalgetticketsController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_tickets";
                    object param = new
                            
                            {
                                @Ticket_id = req.Ticket_id

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetticketsOutput()
                        {
                            Ticket_id = r.Ticket_id,
                            id = r.id == null ? 0 : r.id,
                            Title = r.Title,
                            desc = r.desc,
                            category_id = r.category_id == null ? 0 : r.category_id,
                            category_name = r.category_name,
                            Sub_category_id = r.Sub_category_id == null ? 0 : r.Sub_category_id,
                            sub_category_name = r.sub_category_name,
                            Fk_Status = r.Fk_Status == null ? 0 : r.Fk_Status,
                            Status_name = r.Status_name,
                            Fk_priorty_id = r.Fk_priorty_id == null ? 0 : r.Fk_priorty_id,
                            Priority = r.Priority,
                            created_on = r.created_on == null ? null : r.created_on,
                            create_by = r.create_by,
                            dept_id = r.dept_id == null ? 0 : r.dept_id,
                            dept_name = r.dept_name,
                            attachments = r.attachments,
                            userid = r.userid == null ? 0 : r.userid,
                            username = r.username,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            company_name = r.company_name,
                            comments = r.comments,
                            modify_date = r.modify_date == null ? null : r.modify_date,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetticketsOutput outputobj = new UrcrmportalgetticketsOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetticketsOutput outputobj = new UrcrmportalgetticketsOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }


    }
}
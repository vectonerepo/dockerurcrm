﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static URCRMDocker.Models.UrcrmbillgetcustomerinvoicesummarydtlModel;

namespace URCRMDocker.DB
{
    public class UrcrmbillgetcustomerinvoicesummarydtlDB 
    {

        public static List<UrcrmbillgetcustomerinvoicesummarydtlOutput> CallDB(UrcrmbillgetcustomerinvoicesummarydtlInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmbillgetcustomerinvoicesummarydtlOutput> OutputList = new List<UrcrmbillgetcustomerinvoicesummarydtlOutput>();
            _logger.LogInformation("Input : UrcrmbillgetcustomerinvoicesummarydtlController:" + JsonConvert.SerializeObject(req));
            try
            {
                // using (var conn = new MySqlConnection(_Config["ConnectionStrings:urcrm"]))
                //// using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                //{
                //// conn.Open();
                var sp = "ur_crm_bill_get_customer_invoice_summary_dtl";
                object param = new
                {

                    @company_id = req.company_id,
                    @invoice_id = req.invoice_id
                };
            


                IEnumerable<dynamic> result = null;
                DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");

                
                if (result != null && result.Count() > 0)
                    {
                        _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmbillgetcustomerinvoicesummarydtlOutput()
                        {
                            charge_type = r.charge_type,
                            charge_number = r.charge_number,
                            charge = r.charge == null ? 0.0F : r.charge,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmbillgetcustomerinvoicesummarydtlOutput outputobj = new UrcrmbillgetcustomerinvoicesummarydtlOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmbillgetcustomerinvoicesummarydtlOutput outputobj = new UrcrmbillgetcustomerinvoicesummarydtlOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}

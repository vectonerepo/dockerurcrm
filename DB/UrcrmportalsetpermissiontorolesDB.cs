﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalsetpermissiontorolesModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalsetpermissiontorolesDB
    {
        
        public static List<UrcrmportalsetpermissiontorolesOutput> CallDB(UrcrmportalsetpermissiontorolesInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalsetpermissiontorolesOutput> OutputList = new List<UrcrmportalsetpermissiontorolesOutput>();
           _logger.LogInformation("Input : UrcrmportalsetpermissiontorolesController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_set_permission_to_roles";
                    object param = new
                            
                            {
                                @Role_id = req.Role_id,
                                @features = req.features
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalsetpermissiontorolesOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalsetpermissiontorolesOutput outputobj = new UrcrmportalsetpermissiontorolesOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalsetpermissiontorolesOutput outputobj = new UrcrmportalsetpermissiontorolesOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmgetmacaddressModel;

namespace URCRMDocker.DB
{
    public class UrcrmgetmacaddressDB
    {
        
        public static List<UrcrmgetmacaddressOutput> CallDB(UrcrmgetmacaddressInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmgetmacaddressOutput> OutputList = new List<UrcrmgetmacaddressOutput>();
           _logger.LogInformation("Input : UrcrmgetmacaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_get_mac_address";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id,
                                @referenceid = req.referenceid

                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmgetmacaddressOutput()
                        {
                            dir_users_id = r.dir_users_id == null ? 0 : r.dir_users_id,
                            MAC_address = r.MAC_address,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmgetmacaddressOutput outputobj = new UrcrmgetmacaddressOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                UrcrmgetmacaddressOutput outputobj = new UrcrmgetmacaddressOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetcompanynumberinfoModel;
namespace URCRMDocker.DB
{
    public class urcrmportalgetcompanynumberinfoDB
    {
        
        public static List<urcrmportalgetcompanynumberinfoOutput> CallDB(urcrmportalgetcompanynumberinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetcompanynumberinfoOutput> OutputList = new List<urcrmportalgetcompanynumberinfoOutput>();
           _logger.LogInformation("Input : urcrmportalgetcompanynumberinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_number_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @external_number = req.external_number,
                                @processtype = req.processtype,
                                @id = req.id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetcompanynumberinfoOutput()
                        {
                            External_number = r.External_number,
                            number_type = r.number_type,
                            purchased_date = r.purchased_date == null ? null : r.purchased_date,
                            price = r.price == null ? 0.0F : r.price,
                            purchased_by    = r.purchased_by,
                            Assigned_to = r.Assigned_to,
                            order_id = r.order_id == null ? 0 : r.order_id,
                            upload_path = r.upload_path,
                            main_switchboard = r.main_switchboard == null ? 0 : r.main_switchboard,
                            assign_type = r.assign_type,
                            Hubspot_contactid = r.Hubspot_contactid == null ? 0 : r.Hubspot_contactid,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetcompanynumberinfoOutput outputobj = new urcrmportalgetcompanynumberinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetcompanynumberinfoOutput outputobj = new urcrmportalgetcompanynumberinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }

    }
}
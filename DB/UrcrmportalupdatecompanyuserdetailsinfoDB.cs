﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalupdatecompanyuserdetailsinfoModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalupdatecompanyuserdetailsinfoDB
    {
        
        public static List<UrcrmportalupdatecompanyuserdetailsinfoOutput> CallDB(UrcrmportalupdatecompanyuserdetailsinfoInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalupdatecompanyuserdetailsinfoOutput> OutputList = new List<UrcrmportalupdatecompanyuserdetailsinfoOutput>();
           _logger.LogInformation("Input : UrcrmportalupdatecompanyuserdetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_update_company_user_details_info";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @extension_Number = req.extension_Number,
                                @user_firstname = req.user_firstname,
                                @status = req.status,
                                @user_Surname = req.user_Surname
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalupdatecompanyuserdetailsinfoOutput()
                        {
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalupdatecompanyuserdetailsinfoOutput outputobj = new UrcrmportalupdatecompanyuserdetailsinfoOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalupdatecompanyuserdetailsinfoOutput outputobj = new UrcrmportalupdatecompanyuserdetailsinfoOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
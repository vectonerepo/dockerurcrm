﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrminsertplanchangerequestModel;

namespace URCRMDocker.DB
{
    public class UrcrminsertplanchangerequestDB
    {
        
        public static List<UrcrminsertplanchangerequestOutput> CallDB(UrcrminsertplanchangerequestInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrminsertplanchangerequestOutput> OutputList = new List<UrcrminsertplanchangerequestOutput>();
           _logger.LogInformation("Input : UrcrminsertplanchangerequestController:" + JsonConvert.SerializeObject(req));
            try
            {
                //using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                //{
                  //// conn.Open();
                 var sp = "ur_crm_insert_plan_change_request";
                object param = new
                {
                    @company_id = req.company_id,
                    @product_id = req.product_id,
                    @plan_id = req.plan_id,
                    @plan_duration = req.plan_duration,
                    @bundle_id = req.bundle_id,
                    @plan_price = req.plan_price,
                    @reference_no = req.reference_no,
                    @plan_description = req.plan_description,
                    @adjustment_charge = req.adjustment_charge,
                    @tax_fee = req.tax_fee,
                    @tax_per = req.tax_per,
                    @total_plan_charge = req.total_plan_charge,
                    @userid = req.userid

                };

                IEnumerable<dynamic> result = null; DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm"); if (result != null && result.Count() > 0)


                {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrminsertplanchangerequestOutput()
                        {                          
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrminsertplanchangerequestOutput outputobj = new UrcrminsertplanchangerequestOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                //}
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrminsertplanchangerequestOutput outputobj = new UrcrminsertplanchangerequestOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.urcrmportalgetticketcategoryModel;

namespace URCRMDocker.DB
{
    public class urcrmportalgetticketcategoryDB
    {
        
        public static List<urcrmportalgetticketcategoryOutput> CallDB(urcrmportalgetticketcategoryInput req,IConfiguration _Config, ILogger _logger)
        {
            List<urcrmportalgetticketcategoryOutput> OutputList = new List<urcrmportalgetticketcategoryOutput>();
           _logger.LogInformation("Input : urcrmportalgetticketcategoryController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_ticket_category";
                    object param = new
                            
                            {
                                @category_id = req.category_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urcrmportalgetticketcategoryOutput()
                        {

                            category_id = r.category_id == null ? 0 : r.category_id,
                            category_name = r.category_name,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        urcrmportalgetticketcategoryOutput outputobj = new urcrmportalgetticketcategoryOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                urcrmportalgetticketcategoryOutput outputobj = new urcrmportalgetticketcategoryOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using static URCRMDocker.Models.UrcrmportalgetcompanyinvoiceModel;

namespace URCRMDocker.DB
{
    public class UrcrmportalgetcompanyinvoiceDB
    {
        
        public static List<UrcrmportalgetcompanyinvoiceOutput> CallDB(UrcrmportalgetcompanyinvoiceInput req,IConfiguration _Config, ILogger _logger)
        {
            List<UrcrmportalgetcompanyinvoiceOutput> OutputList = new List<UrcrmportalgetcompanyinvoiceOutput>();
           _logger.LogInformation("Input :  UrcrmportalgetcompanyinvoiceController:" + JsonConvert.SerializeObject(req));
            try
            {
               // using (var conn = new SqlConnection(_Config["ConnectionStrings:urcrm"]))
                {
                  // conn.Open();
                    var sp = "ur_crm_portal_get_company_invoice";
                    object param = new
                            
                            {
                                @company_id = req.company_id,
                                @order_id = req.order_id
                            };
                            
                    IEnumerable<dynamic> result = null;DBConnection.CheckDB.CallDBConnection(_Config, sp, param, ref result, "ConnectionStrings:urcrm");if (result != null && result.Count() > 0)
                    {
                       _logger.LogInformation("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmportalgetcompanyinvoiceOutput()
                        {
                            order_id = r.order_id == null ? 0 : r.order_id,
                            referenceid = r.referenceid,
                            tot_phone_charge = r.tot_phone_charge == null ? 0.0D : r.tot_phone_charge,
                            tot_discount = r.tot_discount == null ? 0.0D : r.tot_discount,
                            tot_tax_fee = r.tot_tax_fee == null ? 0.0D : r.tot_tax_fee,
                            tot_charge = r.tot_charge == null ? 0.0D : r.tot_charge,
                            createdate = r.createdate == null ? null : r.createdate,
                            order_source = r.order_source,
                            approval_date = r.approval_date == null ? null : r.approval_date,
                            tracking_no = r.tracking_no,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            duration_period = r.duration_period,
                            duration_charge = r.duration_charge == null ? 0.0D : r.duration_charge,
                            total_delivery_fee = r.total_delivery_fee == null ? 0.0D : r.total_delivery_fee,
                            adjustment_period = r.adjustment_period,
                            adjustment_charge = r.adjustment_charge == null ? 0.0D : r.adjustment_charge,
                            vat_percetage = r.vat_percetage == null ? 0.0D : r.vat_percetage,
                            vat_charge = r.vat_charge == null ? 0.0D : r.vat_charge,
                            sub_total = r.sub_total == null ? 0.0D : r.sub_total,
                            category = r.category,
                            phone_id = r.phone_id,
                            phone_model = r.phone_model,
                            phone_desc = r.phone_desc,
                            weight = r.weight == null ? 0.0D : r.weight,
                            quantity = r.quantity == null ? 0 : r.quantity,
                            order_with_number = r.order_with_number == null ? 0 : r.order_with_number,
                            order_with_number_price = r.order_with_number_price == null ? 0.0D : r.order_with_number_price,
                            Code = r.errcode == null ? 0 : r.errcode,
                            Message = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                       _logger.LogInformation("Output DB : " + "Empty result");
                        UrcrmportalgetcompanyinvoiceOutput outputobj = new UrcrmportalgetcompanyinvoiceOutput();
                        outputobj.Code = -1;
                        outputobj.Message = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex.Message);
                UrcrmportalgetcompanyinvoiceOutput outputobj = new UrcrmportalgetcompanyinvoiceOutput();
                outputobj.Code = -1;
                outputobj.Message = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}